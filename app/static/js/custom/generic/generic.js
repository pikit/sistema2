$(document).ready(function () {

  not_found_icon()
  
})

var csrftoken = Cookies.get("csrftoken");
function csrfSafeMethod(method) {
  // these HTTP methods do not require CSRF protection
  return /^(GET|HEAD|OPTIONS|TRACE)$/.test(method);
}


messageGeneric = (message, title) => {
  // setTimeout(() => {}, 2000);
  bootbox.alert({
    title: "" + title,
    message: message,
    callback: function (result) {
      // location.href = "../publicaciones";
    },
  });
};

add_icon =  (id,icono) => {
    lottie.loadAnimation({
        container: document.getElementById(id), // the dom element that will contain the animation
        renderer: 'svg',
        loop: true,
        autoplay: true,
        path: icono+'.json' // the path to the animation json
      });
}



not_found_icon  = () => {
  add_icon('not_found',"../../static/iconos/404")
} 