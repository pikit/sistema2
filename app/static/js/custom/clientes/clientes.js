$(document).ready(function () {
    // alert("{% url 'get_municipios' %}")
    $("#id_municipios").prop('disabled', true);
    $("#id_municipios").append('<option  selected value="SINCIUDAD">NA CIUDAD </option>');

  $("#buscador-clientes").keyup(function () {
    input = $(this).val();
    estado = $("#id_estados option:selected").val()
    municipio = $("#id_municipios option:selected").val()

    // console.log(input)

    if (input != "") {
      console.log("escondiendo");
      $("#div_clientes").hide();
      $("#paginador").hide();
      $("#respuesta_ajax").css({ display: "inline" });

      buscador_clientes(input ,estado,municipio)

      // $("#respuesta_ajax").css({'display':'inline'})
    } else {
      $("#div_clientes").show();
      $("#paginador").show();
      $("#respuesta_ajax").css({ display: "none" });

      // $("#paginador").css({'display':'inline'})
      // $("#respuesta_ajax").css({'display':'none'})
    }
    // Show only matching TR, hide rest of them
  });
});

buscador_clientes = (palabra, estado, municipio) => {
  parametros = {
    "palabra": palabra,
    "estado": estado,
    "municipio":municipio,
    "csrfmiddlewaretoken": $("input[name=csrfmiddlewaretoken]").val(),
  };
  


  $.ajax({
    type: "POST",
    url: "buscar-clientes",
    data: parametros,
    beforeSend: function (xhr, settings) {
      if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
        xhr.setRequestHeader("X-CSRFToken", csrftoken);
      }
      // esperando =  wait_response('Guardando producto...')
    },
    success: function (response) {

    resultado  = JSON.parse(response)
    $("#respuesta_ajax").empty()
    if (resultado["status"] == "error") {
        $("#respuesta_ajax").html("<h2 class='text-center' id='leyenda_error'>"+resultado["message"]+"</h2>")
    }else{
        contenido ="<div class='d-flex  flex-wrap   w-100 ' >"
        for (let i = 0; i < resultado["clientes"].length; i++) {
            id = resultado["clientes"][i].id
            filetype = resultado["clientes"][i].filetype
            nombre =  resultado["clientes"][i].nombre
            publicacion_div = ""
            publicacion_div =
            "<div class=' col-lg-4 col-md-5 publicacion'>" +
            "<div class='d-flex flex-column'>" +
            "<figure class='figura'>" +
            "<img src='https://public.pikit.mx/files/logo_empresas/"+id+"."+filetype+"' alt=''>" +
            "</figure>" +
            "<div class='w-100 short_info' style='display: flex;justify-content: center;align-items: center;'>" +
            "<div class='col-md-9 col-sm-11 mx-auto flex-column' style='white-space: nowrap;  display: flex;justify-content: center;align-items: center;'>" +
            "<p class='w-90 text-center mt-2'><a href='cliente/"+id+"' class=''>"+nombre+"</a></p>" +
            "<p class='w-90 text-center '>Cliente: <strong>"+id+"</strong></p>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "</div>";
            contenido+=publicacion_div
            
        }
        contenido+="</div>"
        $("#respuesta_ajax").html(contenido)


    }
      console.log(response);
    },
    error: function (xhr, errmsg, err) {
      alert("Error al solicitar informacion");
      console.log(xhr);
      console.log(errmsg);
    },
  });
};


get_municipios =  (url) => {
estado =$("#id_estados option:selected").val()
$("#id_municipios").empty()
    if (estado != "SINESTADO") {
        
    
   
    parametros = {"estado":estado}
    // url = ''.replace('0', id_number);
    $.ajax({
        type: "POST",
        url: url,
        // url:"get_municipios",
        data: parametros,
        beforeSend: function (xhr, settings) {
          if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
          }
          // esperando =  wait_response('Guardando producto...')
        },
        success: function (response) {
        //   console.log(response);
        $("#id_municipios").prop('disabled', false);
          municipios = JSON.parse(response)
          $("#id_municipios").empty()
          $("#id_municipios").append('<option  selected value="SINCIUDAD">NA CIUDAD </option>');
        for (let index = 0; index < municipios.length; index++) {
            nombre = municipios[index].nombre
            id_municipio=  municipios[index].id_municipio
          $("#id_municipios").append('<option value='+id_municipio+'>'+nombre+'</option>');

            // console.log(nombre)
            
        }
        },
        error: function (xhr, errmsg, err) {
          alert("Error al solicitar informacion");
          console.log(xhr);
          console.log(errmsg);
        },
      });
    }else{
        $("#id_municipios").prop('disabled', true);
    }
}