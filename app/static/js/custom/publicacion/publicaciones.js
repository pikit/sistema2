
$(document).ready(function () {


  $("#btn-publicaciones-revision").keyup(function () {
    _this = this;
    // Show only matching TR, hide rest of them
    $.each($("#div-publicaciones-revision .publicacion"), function () {
      if (
        $(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) ===
        -1
      )
        $(this).hide();
      else $(this).show();
    });
  });

  $("#btn-publicaciones-canceladas").keyup(function () {
    _this = this;
    // Show only matching TR, hide rest of them
    $.each($("#div-publicaciones-canceladas .publicacion"), function () {
      if (
        $(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) ===
        -1
      )
        $(this).hide();
      else $(this).show();
    });
  });
 
  $("#buscador").keyup(function () {

    palabra  =  $(this).val()
    tipo_busqueda = $("#tipo_busqueda_cliente option:selected").val()
    if (palabra  != "") {
      $("#div_main_publicaciones").hide()
      $("#paginador").hide()
      $("#ajax_response_publicaciones").css({ display: "inline" });
      buscador_publicaciones(palabra,tipo_busqueda)
    }else{
      $("#div_main_publicaciones").show()
      $("#paginador").show()
      $("#ajax_response_publicaciones").css({ display: "none" });
    }
    
    
  })


});





buscador_publicaciones = (palabra , tipo_busqueda) =>{
  parametros = {
    "palabra": palabra,
    "tipo_busqueda": tipo_busqueda,
    "csrfmiddlewaretoken": $("input[name=csrfmiddlewaretoken]").val(),
  };
  $.ajax({
    type: "POST",
    url: "../buscar-publicaciones",
    data: parametros,
    beforeSend: function (xhr, settings) {
      if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
        xhr.setRequestHeader("X-CSRFToken", csrftoken);
      }
      // esperando =  wait_response('Guardando producto...')
    },
    success: function (response) {
    console.log(response);
    respuesta =  JSON.parse(response)
    if(respuesta["status"] == "error"){
      $("#ajax_response_publicaciones").html("<h2 class='text-center' id='leyenda_error'>"+resultado["message"]+"</h2>")
    }else{
      main_div = "<div class='d-flex  flex-wrap   w-100  justify-content-center align-items-center' id='div_publicaciones'>"

      for (let i = 0; i < respuesta["publicaciones"].length; i++) {
        id_publicacion = respuesta["publicaciones"][i].id_publicacion
        filetype_publicacion = respuesta["publicaciones"][i].filetype_publicacion
        cliente  = respuesta["publicaciones"][i].cliente
        id_cliente =respuesta["publicaciones"][i].id_cliente
        filetype_cliente =  respuesta["publicaciones"][i].filetype_cliente
        titulo = respuesta["publicaciones"][i].titulo
        fecha  = respuesta["publicaciones"][i].fecha
        div_publicacion = ""
        div_publicacion  = "<div class=' col-lg-4 col-md-8 publicacion'>"+
        "<div class='d-flex flex-column'>"+
           " <figure class='figura'>"+
              "<img src='https://public.pikit.mx/files/publicaciones/"+id_publicacion+"."+filetype_publicacion+"' alt=''>"+
           " </figure>"+
            "<div class='w-100 short_info'>"+
             " <div class='d-flex  justify-content-center align-items-center ' style='cursor: pointer;' title='"+cliente+"'>"+
               " <figure class='fig_circulo'   style='background-image: url(https://public.pikit.mx/files/logo_empresas/"+id_cliente+"."+filetype_cliente+");'>"+
              "  </figure>"+
                "<div class='col-md-9 col-sm-11' style='white-space: nowrap;'>"+
                "<p class='w-90 text-center titulo_pub'><a href='../publicacion/"+id_publicacion+"'  class='' > "+titulo+"</a></p>"+
                "<p class='w-90 text-center fecha_pub' style=''>"+fecha+"</p>"+
                "</div>"+
              "</div>"+
            "</div>"+
        "</div>"+
    "</div>"
    main_div+=div_publicacion
        
      }
      main_div+="</div>"
      $("#ajax_response_publicaciones").html(main_div)

    }

    },
    error: function (xhr, errmsg, err) {
      alert("Error al solicitar informacion");
      console.log(xhr);
      console.log(errmsg);
    },
  });
}


var revision_pub = (id) => {
  bootbox.prompt({
    title: "Por favor escriba motivo de revisión",
    inputType: "textarea",
    buttons: {
      confirm: {
        label: "Revisión",
        className: "btn-success",
      },
      cancel: {
        label: "Cancelar",
        className: "btn-danger",
      },
    },
    callback: function (result) {
      if (result) {
        parametros = {
          id: id,
          explicacion: result,
          csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
        };
        // console.log(parametros)
        Swal.fire({
          html: '<p>Pasando a revisión y enviado notificación a cliente...</p>',
          timerProgressBar: true,
          onOpen: function () {
            Swal.showLoading()
            $.ajax({
              type: "POST",
              url: "../enviarRevision",
              data: parametros,
              beforeSend: function (xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                  xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
                // esperando =  wait_response('Guardando producto...')
              },
              success: function (response) {
                console.log(response);
                resp = JSON.parse(response);
                mensaje = "";
                // <i class="fas fa-times-circle"></i>
                Swal.fire({
                  title: 'Publicaciones',
                  text: ''+resp["message"],
                  icon: ''+resp["status"],
                  confirmButtonText: 'Aceptar',
                  allowOutsideClick:false
                }).then((result)=>{
                  if (result.value) {
                    location.href = "../publicaciones";
                  }
                })
    
    
                // messageGeneric(mensaje, "Revisión");
                console.log(resp);
              },
              error: function (xhr, errmsg, err) {
                alert("Error al solicitar informacion");
                console.log(xhr);
                console.log(errmsg);
              },
            });
      
      
          }
        })

      } else {
        // alert("cancelado")
      }
    },
  });
};

var cancelar_pub = (id) => {
  bootbox.prompt({
    title: "Por favor escriba motivo de rechazo",
    inputType: "textarea",
    buttons: {
      confirm: {
        label: "Rechazar",
        className: "btn-success",
      },
      cancel: {
        label: "Cancelar",
        className: "btn-danger",
      },
    },
    callback: function (result) {
      if (result) {
        parametros = {
          id: id,
          explicacion: result,
          csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
        };
        // console.log(parametros)
        Swal.fire({
          html: '<p>Cancelando publicación  y enviado notificación a cliente...</p>',
          timerProgressBar: true,
          onOpen: function () {
            Swal.showLoading()

            $.ajax({
              type: "POST",
              url: "../enviarCancelacion",
              data: parametros,
              beforeSend: function (xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                  xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
                
              },
              success: function (response) {
    
                resp = JSON.parse(response);
                mensaje = "";
                Swal.fire({
                  title: 'Publicaciones',
                  text: ''+resp["message"],
                  icon: ''+resp["status"],
                  confirmButtonText: 'Aceptar',
                  allowOutsideClick:false
                }).then((result)=>{
                  if (result.value) {
                    location.href = "../publicaciones";
                  }
                })
              },
              error: function (xhr, errmsg, err) {
                alert("Error al solicitar informacion");
                console.log(xhr);
                console.log(errmsg);
              },
            });
          }
        })

      } else {
        // alert("cancelado")
      }
    },
  });
};

aceptar_publicacion = (id) => {
  bootbox.confirm({
    message: "¿Estas seguro de aprobar esta publicación? ",
    buttons: {
      confirm: {
        label: "Si",
        className: "btn-success",
      },
      cancel: {
        label: "No",
        className: "btn-danger",
      },
    },
    callback: function (result) {
      parametros = {
        id: id,
        csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
      };
      // console.log(parametros)
      if (result) {
        $.ajax({
          type: "POST",
          url: "../aceptadas",
          data: parametros,
          beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
              xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
            // esperando =  wait_response('Guardando producto...')
          },
          success: function (response) {
            console.log(response);
            resp = JSON.parse(response);
            mensaje = "";
            Swal.fire({
              title: 'Publicaciones',
              text: ''+resp["message"],
              icon: ''+resp["status"],
              confirmButtonText: 'Aceptar',
              allowOutsideClick:false
            }).then((result)=>{
              if (result.value) {
                location.href = "../publicaciones";
              }
            })

            // messageGeneric(mensaje, "Revisión");
            // console.log(resp);
          },
          error: function (xhr, errmsg, err) {
            alert("Error al solicitar informacion");
            console.log(xhr);
            console.log(errmsg);
          },
        });
      }
    },
  });
};

messageGeneric = (message, title) => {
  // setTimeout(() => {}, 2000);
  bootbox.alert({
    title: "" + title,
    message: message,
    callback: function (result) {
      location.href = "../publicaciones";
    },
  });
};

// Write on keyup event of keyword input element

buscador = () =>{
  // $( "#buscador" ).keyup(function() {
    // console.log($(this).val())

  //   if ($(this).val() != "") {
  //     $("#div_publicaciones").empty()
  //     $("#paginador").empty()
  //   } else {
  //     console.log("vacio")
  //   }

  // });


  var search = document.getElementById("buscador"),
    food = $(".publicacion"),
    forEach = Array.prototype.forEach;

    // console.log(food)

search.addEventListener("keyup", function(e){
    var text = this.value;
  
    forEach.call(food, function(f){
        if (f.innerHTML.toLowerCase().search(text.toLowerCase()) == -1)
        {

            f.parentNode.style.display = "none"; 
        }
                 
        else{

          // f.parentNode.style.display = "block"; 
        }
                   
    });
}, false);
publicaciones  =  $('div.publicacion').length
console.log(f.innerHTML)
}

 var get_publicaciones = () =>{

}




function importar_pdf(campo, type_file) {

	//'requisicion','requisicion'
	datos = $("#" + campo).val();

	param = {
		"datos": datos,
		"tipo": type_file
	}


}