$(document).ready(function () {

  // $("#id_estatus option:eq(2)").attr("selected",true);



  $("#btn_guardar_usuario").on("click", function (e) {
    vacios = 0;
    e.preventDefault();
    datos = [];
    var dataString = $("#signup_form").serializeArray();
    for (let i = 0; i < dataString.length; i++) {
      //   console.log(dataString[i])
      datos.push(dataString[i].value);
      if (dataString[i].value == "") {
        vacios++;
        //   $("#id_"+dataString[i].name).addClass('error')
      }
    }
    //   console.log(datos)
    parametros = {
      datos: datos,
      csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
      action: "post",
    };

    // console.log(parametros);
    var esperando = "";
    if (vacios == 0) {
      $.ajax({
        type: "POST",
        url: "registro-usuarios",
        data: parametros,
        beforeSend: function (xhr, settings) {
          if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
          }
        },
        success: function (response) {
          resp = JSON.parse(response);

          Swal.fire({
            title: "Registro de usuario",
            text: "" + resp["message"],
            icon: "" + resp["status"],
            confirmButtonText: "Aceptar",
            allowOutsideClick:false
          }).then((result) => {
            if (result.value) {
              if (resp["status"] == "success") {
                location.href="listado-usuarios"
              }
              
            }
          });
        },
        error: function (xhr, errmsg, err) {
          alert("Error al solicitar informacion");
          // console.log(xhr);
          // console.log(errmsg);
        },
      });
    } else {
      Swal.fire({
        title: "Registro de usuario",
        text: "Campos vacíos",
        icon: "error",
        confirmButtonText: "Aceptar",
      });
    }
  });


  $("#btn-modificar-usuario").on("click", function (e) {


    vacios = 0;
    e.preventDefault();
    datos = [];
    var dataString = $("#update_form_user").serializeArray();
    for (let i = 0; i < dataString.length; i++) {
      //   console.log(dataString[i])
      datos.push(dataString[i].value);
      if (dataString[i].value == "") {
        vacios++;
        //   $("#id_"+dataString[i].name).addClass('error')
      }
    }
    //   console.log(datos)
    parametros = {
      datos: datos,
      csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
      action: "post",
    };

    // console.log(parametros);
    var esperando = "";
    if (vacios  == 0) {
      console.log(parametros)
      mensaje  = "¿ Esta seguro que desea modificar el usuario?"
      bootbox.confirm({
        message:mensaje
          ,
        buttons: {
          confirm: {
            label: "Si",
            className: "btn-success",
          },
          cancel: {
            label: "No",
            className: "btn-danger",
          },
        },
        callback: function (result) {
          if (result) {
            $.ajax({
              type: "POST",
              url: "../metodoActualizar",
              data: parametros,
              beforeSend: function (xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                  xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
              },
              success: function (response) {
                console.log(response)
                resp = JSON.parse(response);
      
                Swal.fire({
                  title: "Modificación de usuario",
                  text: "" + resp["message"],
                  icon: "" + resp["status"],
                  confirmButtonText: "Aceptar",
                  allowOutsideClick:false
                }).then((result) => {
                  if (result.value) {
                    if (resp["status"] == "success") {
                      location.href="../listado-usuarios"
                    }
                  }
                });
              },
              error: function (xhr, errmsg, err) {
                alert("Error al solicitar informacion");
                // console.log(xhr);
                // console.log(errmsg);
              },
            });
          }
        }
      })

    } else {
      Swal.fire({
        title: "Modificación de usuario",
        text: "Campos vacíos",
        icon: "error",
        confirmButtonText: "Aceptar",
      });
    }



  });


  $("#btn_buscar_usuarios").keyup(function () {
    _this = this;
    // Show only matching TR, hide rest of them
    $.each($("#tabla_usuarios tbody tr"), function () {
      if (
        $(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) ===
        -1
      )
        $(this).hide();
      else $(this).show();
    });
  });
});

// Write on keyup event of keyword input element

messageGeneric = (message, title) => {
  // setTimeout(() => {}, 2000);
  bootbox.alert({
    title: "" + title,
    message: message,
    callback: function (result) {
      // location.href = "../publicaciones";
    },
  });
};

cambiar_estatus_usuario = (id, usuario,estatus) => {
  parametros = {"id":id,csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),"estatus":estatus}
  mensaje  = ""
  if (estatus  == 0 ) { 
   mensaje =  "¿ Estas seguro de desactivar al usuario <strong>" + usuario + "</strong>"
  }
  if (estatus  ==  1) {
    mensaje =  "¿ Estas seguro de activar  al usuario <strong>" + usuario + "</strong>"
  }
  bootbox.confirm({
    message:mensaje
      ,
    buttons: {
      confirm: {
        label: "Si",
        className: "btn-success",
      },
      cancel: {
        label: "No",
        className: "btn-danger",
      },
    },
    callback: function (result) {
      if (result) {
        $.ajax({
          type: "POST",
          url: "desactivar_usuario",
          data: parametros,
          beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
              xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
          },
          success: function (response) {
            resp = JSON.parse(response);
            // console.log(resp)
            Swal.fire({
              title: "Usuarios",
              text: "" + resp["message"],
              icon: "" + resp["status"],
              confirmButtonText: "Aceptar",
            }).then((result) => {
              if (result.value) {
                location.href='listado-usuarios'
              }
            });
          },
          error: function (xhr, errmsg, err) {
            alert("Error al solicitar informacion");
            // console.log(xhr);
            // console.log(errmsg);
          },
        });
      } else {
      }
    },
  });

  // alert(id)
};


grupos_usuarios =  () =>{
  // estado = $("#id_estados option:selected").val()

  tipo_usuario =  $("#id_tipo_usuario option:selected").val()
  estatus  = 0
  // gerente de ventas
  if (tipo_usuario  == 3) {
    estatus  = 1
    
  }
  // vendedor
  else if (tipo_usuario == 4){
    estatus = 0
  }
  else{
    estatus  = 2
  }
  
  x =  $("#id_estatus option:selected").val()

console.log(x)
  $("#id_estatus option:eq("+estatus+")").attr("selected",true);
  

}