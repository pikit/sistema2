$(document).ready(function () {
  $("#btn_buscar_vendedores").keyup(function () {
    _this = this;
    // Show only matching TR, hide rest of them
    $.each($("#tabla_vendedores tbody tr"), function () {
      if (
        $(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) ===
        -1
      )
        $(this).hide();
      else $(this).show();
    });
  });

  $("#btn-buscador-clientes-asignacion").keyup(function () {
    palabra = $(this).val();
    if (palabra == "") {
      $("#clientes-encontrados").find("tbody").empty();
    } else {
      estado = $("#id_estados option:selected").val();
      municipio = $("#id_municipios option:selected").val();
      parametros = { palabra: palabra, estado: estado, municipio: municipio };
      $.ajax({
        type: "POST",
        url: "../../clientes/buscar-clientes",
        data: parametros,
        beforeSend: function (xhr, settings) {
          if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
          }
        },
        success: function (response) {
          console.log(response);
          resp = JSON.parse(response);
          $("#clientes-encontrados").find("tbody").empty();
          if (resp["status"] == "error") {
            $("#clientes-encontrados")
              .find("tbody")
              .append('<tr><td colspan="3">' + resp["message"] + "</td></tr>");
          } else {
            for (let i = 0; i < resp["clientes"].length; i++) {
              id_cliente = resp["clientes"][i].id;
              nombre = resp["clientes"][i].nombre;
              vendedor_asignado = resp["clientes"][i].vendedor_asignado
              vendedor =  resp["clientes"][i].vendedor
              if (vendedor_asignado) {
                $("#clientes-encontrados")
                .find("tbody")
                .append(
                  '<tr><td><i class="fas fa-check-circle"></i></td><td>' +
                    id_cliente +
                    "</td><td>" +
                    nombre +"</td>"+
                    "<td>"+vendedor+"</td></tr>"
                );
              } else {
              $("#clientes-encontrados")
                .find("tbody")
                .append(
                  '<tr><td><div class="form-check form-check-inline"><input class="form-check-input" type="checkbox" value="" id="' +
                    id_cliente +
                    '"></div></td><td>' +
                    id_cliente +
                    "</td><td>" +nombre +"</td>"+
                    "<td>"+vendedor+"</td></tr>"
                );
              }

            }
          }
        },
        error: function (xhr, errmsg, err) {
          alert("Error al solicitar informacion");
          // console.log(xhr);
          // console.log(errmsg);
        },
      });
    }
  });
});

cambiar_estatus_vendedor = (id, vendedor, estatus) => {
  parametros = {
    id: id,
    csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
    estatus: estatus,
  };
  mensaje = "";
  if (estatus == 0) {
    mensaje =
      "¿ Estas seguro de desactivar al vendedor <strong>" +
      vendedor +
      "?</strong>";
  }
  if (estatus == 1) {
    mensaje =
      "¿ Estas seguro de activar  al vendedor <strong>" +
      vendedor +
      "?</strong>";
  }

  bootbox.confirm({
    message: mensaje,
    buttons: {
      confirm: {
        label: "Si",
        className: "btn-success",
      },
      cancel: {
        label: "No",
        className: "btn-danger",
      },
    },
    callback: function (result) {
      if (result) {
        $.ajax({
          type: "POST",
          url: "desactivar_vendedor",
          data: parametros,
          beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
              xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
          },
          success: function (response) {
            resp = JSON.parse(response);
            // console.log(resp)
            Swal.fire({
              title: "Vendedores",
              text: "" + resp["message"],
              icon: "" + resp["status"],
              confirmButtonText: "Aceptar",
            }).then((result) => {
              if (result.value) {
                location.href = "listado-vendedores";
              }
            });
          },
          error: function (xhr, errmsg, err) {
            alert("Error al solicitar informacion");
            // console.log(xhr);
            // console.log(errmsg);
          },
        });
      }
    },
  });
};

get_table = (vendedor,id_vendedor) => {
  var $data = [];
  $("#clientes-encontrados tbody tr ")
    .find("td:eq(0) input[type=checkbox]")
    .each(function (i) {
      if ($(this).prop("checked")) {
        $data.push($(this).attr("id"));
      }
    });
console.log($data)
  if ($data.length > 0) {
    parametros = {
      "clientes": $data,
      "csrfmiddlewaretoken": $("input[name=csrfmiddlewaretoken]").val(),
      "id_vendedor":id_vendedor
    };

    bootbox.confirm({
      message: "¿Seguro de asignar los clientes al vendedor <strong>"+vendedor+"</strong>?",
      buttons: {
        confirm: {
          label: "Si",
          className: "btn-success",
        },
        cancel: {
          label: "No",
          className: "btn-danger",
        },
      },
      callback: function (result) {
        if (result) {
          $("#btn-modal-cerrar").click()
          $.ajax({
            type: "POST",
            url: "../asignar-vendedor",
            data: parametros,
            beforeSend: function (xhr, settings) {
              if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
              }
            },
            success: function (response) {
              console.log(response)
              resp = JSON.parse(response);
              Swal.fire({
                title: "Vendedores",
                text: "" + resp["message"],
                icon: "" + resp["status"],
                confirmButtonText: "Aceptar",
              }).then((result) => {
                if (result.value) {
                  if (resp["status"] == "success") {
                    $("#div-clientes-por-vendedor").load(location.href + " #div-clientes-por-vendedor");
                  }
                  
                  // location.href = "listado-vendedores";
                  // $("#btn-modal-cerrar").click()
                }
              });
            },
            error: function (xhr, errmsg, err) {
              alert("Error al solicitar informacion");
            },
          });
        }
      },
    });
  } else {
    Swal.fire({
      title: "Asignación de clientes",
      text: "Debe seleccionar al menos 1 cliente.", 
      icon: "warning",
      confirmButtonText: "Aceptar",
    })
  }
};


clear_table  = () =>{
  $("#btn-buscador-clientes-asignacion").val("")
  $("#clientes-encontrados").find("tbody").empty();
}



delete_cliente_vendedor = (id_cliente, vendedor) => {
  parametros = {
    "id_cliente":id_cliente
  ,"csrfmiddlewaretoken": $("input[name=csrfmiddlewaretoken]").val()}


  bootbox.confirm({
    message: "¿Seguro que deseas eliminar al cliente  del vendedor <strong>"+vendedor+"</strong>?",
    buttons: {
      confirm: {
        label: "Si",
        className: "btn-success",
      },
      cancel: {
        label: "No",
        className: "btn-danger",
      },
    },
    callback: function (result) {
      if (result) {
        $("#btn-modal-cerrar").click()
        $.ajax({
          type: "POST",
          url: "../eliminar-cliente-vendedor",
          data: parametros,
          beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
              xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
          },
          success: function (response) {
            console.log(response)
            resp = JSON.parse(response);
            Swal.fire({
              title: "Vendedores",
              text: "" + resp["message"],
              icon: "" + resp["status"],
              confirmButtonText: "Aceptar",
            }).then((result) => {
              if (result.value) {
                if (resp["status"] == "success") {
                  $("#div-clientes-por-vendedor").load(location.href + " #div-clientes-por-vendedor");
                }
                
                // location.href = "listado-vendedores";
                // $("#btn-modal-cerrar").click()
              }
            });
          },
          error: function (xhr, errmsg, err) {
            alert("Error al solicitar informacion");
          },
        });
      }
    },
  });
}