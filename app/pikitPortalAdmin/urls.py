"""pikitPortalAdmin URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from django.contrib.auth import login,logout
from django.views.generic.base import TemplateView # new
from administrationProfile import views
from usuarios import views as seccion_usuarios


urlpatterns = [
    path('',seccion_usuarios.user_login, name="customLogin"),
    path('admin/', admin.site.urls),
    path('administrationProfile/', include('administrationProfile.urls')),
    path('clientes/',include('clientes.urls')),
    path('usuarios/',include('usuarios.urls')),
    path('vendedores/',include('vendedores.urls')),
    path('portal/',include('generic.urls')),
    path('', include('django.contrib.auth.urls')),
    path('', login,{'template_name':'templates/registration/login.html'}, name='home'),
    path('emoji/', include('emoji.urls')),
]
