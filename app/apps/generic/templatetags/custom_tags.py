from django import template
from clientes.models import *
from django.template.defaultfilters import stringfilter

register =  template.Library()
# register.filter('reparar', encode('latin1').decode('utf8'))

@register.simple_tag
def clientes_count():
    return Clientes.objects.count()


@register.filter
@stringfilter
def reparar_utf8(palabra):
    reparado = ""
    try:
        reparado =  palabra.encode('latin1').decode('utf8')
    except :
        reparado =  palabra

    return reparado

@register.filter
@stringfilter
def mayusculas(palabra):
    reparado =  palabra.lower()
    reparado =  reparado.capitalize()
    return reparado