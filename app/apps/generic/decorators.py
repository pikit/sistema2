from django.http import HttpResponse,HttpResponseBadRequest
from django.shortcuts import redirect
from generic.models import *
from .models import *
from usuarios.models import *
import json
# https://www.youtube.com/watch?v=eBsc65jTKvw
# https://es.stackoverflow.com/questions/1072/heredando-de-abstractuser-django-admin-no-hashea-passwords-useradmin-no-mues
# https://es.stackoverflow.com/questions/930/roles-de-usuarios-en-django
def permitir_secciones_usuarios(grupos_permitidos=[]):
    def decorador(view_func):
        def wrapper_func(request, *args, **kwargs):
            
            group = None
            if AuthUser.objects.filter(id=request.session['id']).exists():
                grupos_usuario = ""
                if AuthUserGroups.objects.filter(user_id=request.session['id']).exists() == False:
                    return HttpResponse('No tienes ningun grupo asignado')
                usuario_G =  AuthUserGroups.objects.select_related('group').get(user_id=request.session['id'])
                grupos_usuario = usuario_G.group.name
            if grupos_usuario in grupos_permitidos:
                return view_func(request, *args, **kwargs)
            else:
                return HttpResponse('Tu no tienes permitido esta seccion')


            return view_func(request, *args, **kwargs)

        return wrapper_func
    return decorador



def ajax_required(f):
    def wrap(request, *args, **kwargs):
        if not request.is_ajax():
            respuesta  = {"status":"error","message":"it isn't ajax request"}
            return HttpResponseBadRequest(json.dumps(respuesta))
        # return ajax_required(request, *args, **kwargs)
        # else:
            # respuesta  = {"status":"error","message":"it is ajax request"}
            # return HttpResponse(json.dumps(respuesta))


    # wrap.__doc__=f.__doc__
    # wrap.__name__=f.__name__
    return wrap

# def gila(parameter_list):
#     return 
# return f(request, *args, **kwargs)

def permitir_acciones_usuarios(grupos_permitidos=[]):
    def customDecorador(view_func):
        def wrapper(request, *args,**kwargs):
            pass
        return customDecorador






