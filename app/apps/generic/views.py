

# Create your views here.
from django.shortcuts import render
from django.http import HttpResponse
from generic.models import *
import json
import sys
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
import json
from django.db.models import Q
# from django.utils import timezon
import datetime
from generic.models import *



# def publicaciones(request):
#     total =  Publicaciones.objects.all().count()
#     # publicaciones_datos = Publicaciones.objects.filter(date_insert__startswith=date)
#     publicaciones_datos = Publicaciones.objects.filter(date_insert__year=2019,date_insert__month=5).order_by('titulo')[:10]

#     # YourModel.objects.filter(datetime_published=datetime(2008, 03, 27))

#     datos  = {"total":total,"publicaciones":publicaciones_datos}
#     return datos

def reparar_utf8(palabra):
    reparado = ""
    try:
        reparado =  palabra.encode('latin1').decode('utf8')
    except :
        reparado =  palabra

    return reparado

@login_required(login_url='customLogin')
def pagina_pricipal(request):
    
    return render(request, "generic/pagina_principal.html")