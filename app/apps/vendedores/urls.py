from django.conf.urls import url
from django.urls import path
from . import views
urlpatterns = [
    path('clientes-por-vendedor/<int:id_vendedor>',views.clientes_por_vendedor, name="clientes_por_vendedor"),
    path('listado-vendedores',views.listado_vendedores, name = "listado_vendedores"), 
    path('desactivar_vendedor',views.desactivar_vendedor,name="desactivar_vendedor"),
    path('asignar-vendedor',views.asignar_vendedor,name="asignacion_vendedor"),
    path('eliminar-cliente-vendedor',views.eliminar_cliente_vendedor,name="eliminar_cliente_vendedor"),
    path('mis-clientes/<int:id_usuario>',views.mis_clientes, name="mis_clientes"),


    # path('enviarRevision', EntityAjaxView.as_view(), name="enviar_revision")  mis_clientes
]