from django.db import models
from administrationProfile.models import *
# Create your models here.



class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'

class Vendedores(models.Model):
    usuario  =  models.OneToOneField(AuthUser,on_delete=models.CASCADE)
    is_gerente =  models.BooleanField(default=False)
    is_activo = models.BooleanField(default=False)

    class Meta:
        managed = True
        db_table  =  'vendedores'

class ClientesPorVendedor(models.Model):
    vendedor  = models.ForeignKey(Vendedores,on_delete=models.CASCADE)
    cliente =  models.ForeignKey(Clientes,on_delete=models.CASCADE)
    asigno = models.IntegerField()
    fecha  =  models.DateTimeField()

    class Meta:
        managed = True
        db_table = "clientes_por_vendedor"
