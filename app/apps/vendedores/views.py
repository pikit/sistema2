from django.shortcuts import render
from django.http import HttpResponse, HttpResponseBadRequest
from django.contrib.auth.decorators import login_required
from .models import *
from generic.models import *
import json
import datetime
from datetime import timedelta
from django.utils import timezone
from generic.decorators import *
# Create your views here.


@permitir_secciones_usuarios(grupos_permitidos=['Administrador', 'GerenteVentas', 'Vendedor'])
@login_required(login_url='customLogin')
def listado_vendedores(request):
    vendedores = Vendedores.objects.select_related(
        'usuario').all().order_by('id')
    datos = {"vendedores": vendedores, "query": vendedores.query}
    return render(request, "vendedores/listado.html", datos)


@permitir_secciones_usuarios(grupos_permitidos=['Administrador', 'GerenteVentas', 'Vendedor'])
@login_required(login_url='customLogin')
def clientes_por_vendedor(request, id_vendedor):
    # clientes =  ClientesPorVendedor.objects.select_related('cliente').filter(vendedor_id=id_vendedor)count()

    try:
        vendedores = Vendedores.objects.select_related(
            'usuario').get(id=id_vendedor)

    except Vendedores.DoesNotExist:
        datos = {"existe_vendedor": False,
                 "message": "No existe el vendedor con el numero "+str(id_vendedor)}
        return render(request, "vendedores/clientes_por_vendedor.html", datos)

    clientes = ClientesPorVendedor.objects.select_related(
        'cliente').filter(vendedor_id=id_vendedor)
    resultados = False
    message_resultados = "Sin clientes asignados :("
    if not len(clientes) == 0:
        resultados = True

    query = clientes.query
    estados = Estados.objects.all()
    datos = {"existe_vendedor": True,
             "resultados": resultados,
             "message_resultados": message_resultados,
             "clientes": clientes,
             "vendedor": vendedores,
             "query": clientes.query,
             "id_vendedor": id_vendedor,
             "estados": estados
             }
    return render(request, "vendedores/clientes_por_vendedor.html", datos)


def desactivar_vendedor(request):
    if request.method == "POST":
        id_usuario = request.POST['id']
        estatus = int(request.POST['estatus'])
        try:
            vendedor = Vendedores.objects.get(usuario_id=id_usuario)
            vendedor.is_activo = estatus
            try:
                vendedor.save()
                mensaje = ""
                if estatus == 1:
                    mensaje = "Vendedor activado"
                else:
                    mensaje = "Vendedor desactivado"
                datos = {"status": "success", "message": mensaje}
                return HttpResponse(json.dumps(datos))
            except:

                datos = {"status": "error",
                         "message": "No se logro actualizar el usuario "}
                return HttpResponseBadRequest(json.dumps(datos))

        except Vendedores.DoesNotExist:
            datos = {"status": "error", "message": "El vendedor no existe"}
            return HttpResponseBadRequest(json.dumps(datos))

    else:
        datos = {"status": "error", "message": "Bad request"}
        return HttpResponseBadRequest(json.dumps(datos))


def asignar_vendedor(request):
    if not request.is_ajax():
        datos = {"status": "error", "message": "Debe ser una solicitud AJAX"}
        return HttpResponseBadRequest(json.dumps(datos))

    if request.method == "POST":
        if not request.user.is_authenticated:
            datos = {"status": "error", "message": "Usuario no logeado"}
            return HttpResponseBadRequest(json.dumps(datos))
        if not request.session['is_administrador'] or request.session['is_gerente_ventas']:
            datos = {"status": "error",
                     "message": "No estas autorizado para  eliminar clientes de los vendedores"}
            return HttpResponse(json.dumps(datos))

        clientes = request.POST.getlist('clientes[]')
        id_vendedor = request.POST['id_vendedor']
        vendedor = Vendedores.objects.get(id=id_vendedor)
        if not vendedor.is_activo:
            datos = {"status": "info",
                     "message": "El vendedor se encuentra desactivado no se le puede asignar clientes"}
            return HttpResponse(json.dumps(datos))
        contador = 0
        for cliente in clientes:
            crear = ClientesPorVendedor.objects.create(
                asigno=request.session['id'],
                fecha=datetime.datetime.now(tz=timezone.utc),
                cliente_id=cliente,
                vendedor_id=id_vendedor,
            )
            if crear:
                contador = contador + 1
                print(contador)

        if not contador == len(clientes):
            datos = {"status": "error",
                     "message": "No se logro asignar los clientes. Intente mas tarde"}
            return HttpResponse(json.dumps(datos))
        else:
            datos = {"status": "success", "message": "Clientes asignados"}
            return HttpResponse(json.dumps(datos))

    else:
        datos = {"status": "error", "message": "Bad request"}
        return HttpResponseBadRequest(json.dumps(datos))


def eliminar_cliente_vendedor(request):
    if not request.is_ajax():
        datos = {"status": "error", "message": "Debe ser una solicitud AJAX"}
        return HttpResponseBadRequest(json.dumps(datos))
    if request.method == "POST":
        id_cliente = request.POST['id_cliente']
        if not request.session['is_administrador'] or request.session['is_gerente_ventas']:
            datos = {"status": "error",
                     "message": "No estar autorizado eliminar clientes de los vendedores"}
            return HttpResponse(json.dumps(datos))
        try:
            cliente = ClientesPorVendedor.objects.get(cliente_id=id_cliente)
            try:
                cliente.delete()
                datos = {"status": "success", "message": "Cliente eliminado"}
                return HttpResponse(json.dumps(datos))
            except:
                datos = {"status": "error",
                         "message": "No se logro eliminar el cliente"}
                return HttpResponse(json.dumps(datos))
        except ClientesPorVendedor.DoesNotExist:
            datos = {"status": "error",
                     "message": "La publicacion no se encuentra asignada a ningun vendedor"}
            return HttpResponse(json.dumps(datos))

    else:
        datos = {"status": "error", "message": "Bad request"}
        return HttpResponseBadRequest(json.dumps(datos))


@permitir_secciones_usuarios(grupos_permitidos=['Administrador', 'GerenteVentas', 'Vendedor'])
@login_required(login_url='customLogin')
def mis_clientes(request, id_usuario):
    if not id_usuario  ==  request.session['id']:
        datos = {"iguales":False,"message":"Solo puedes consultar tus propios clientes"}
        return render(request,"vendedores/mis_clientes.html",datos)
    try:
        datos_vendedor  = Vendedores.objects.get(usuario_id =  id_usuario)
        datos_clientes = ClientesPorVendedor.objects.select_related(
                'cliente').filter(vendedor_id=datos_vendedor.id)
        numero_clientes = len(datos_clientes)
        if not numero_clientes > 0:
            datos = {"iguales":True,"vendedor":True,
            "cliente": False,
             "message": "Sin clientes asignados",
             "datos_vendedor":datos_vendedor}
            return render(request, "vendedores/mis_clientes.html", datos)

            
        datos = {"iguales":True,"vendedor":True,
            "cliente": True,
             "datos_clientes": datos_clientes,
             "datos_vendedor":datos_vendedor}
        return render(request, "vendedores/mis_clientes.html", datos)
    except Vendedores.DoesNotExist:
        datos = {"vendedor":False,"message":"No eres un Gerente de ventas o vendedor"}
        return render(request,"vendedores/mis_clientes.html",datos)

