from django.conf.urls import url
from django.urls import path
from . import views
urlpatterns = [
    path('publicacion/<int:id_publicacion>',views.publicacion_detallada, name="publicacion"),
    path('publicaciones/',views.publicaciones, name = "publicaciones"), 
    path('enviarRevision',views.enviar_Revision, name="enviar_Revision"),
    path('en-revision',views.publicaciones_revision, name="en_revision"),
    path('enviarCancelacion',views.enviar_cancelacion, name="enviar_Cancelacion"),
    path('canceladas',views.canceladas, name="canceladas"),
    path('aceptadas',views.aceptar_publicaciones, name="aceptadas"),
    path('aprobadas',views.publicaciones_aceptadas, name="aprobadas"),
    path('buscar-publicaciones',views.buscar_publicaciones, name="aprobadas"),

    # path('enviarRevision', EntityAjaxView.as_view(), name="enviar_revision")
]