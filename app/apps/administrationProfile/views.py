# -*- coding: utf8 -*-
import os
import sys
from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from administrationProfile.models import *
import json
import config
# import sys
from django.views.decorators.csrf import csrf_protect
import json
from django.db.models import Q
from django.core.paginator import Paginator
from generic.views import *
import datetime
from datetime import timedelta
from django.utils import timezone
from django.db import connection
import MySQLdb as mdb
from urllib.request import urlopen
from django.db.models import ForeignKey
from django.contrib.auth.decorators import login_required
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
import random

from generic.decorators import * 

@login_required(login_url='customLogin')
@permitir_secciones_usuarios(grupos_permitidos=['Administrador','Supervisor'])
def publicaciones(request):

    clientes = ClientesUsuarios.objects.all()

#     # publicaciones_datos = Publicaciones.objects.filter(date_insert__startswith=date)
    # publicaciones_datos = Publicaciones.objects.filter(date_insert__year=2019,date_insert__month=5).order_by('titulo')[:10]
    # publicaciones_datos =  Publicaciones.objects.exclude(
    #     id__in=Publicaciones_revision.objects.filter(fecha_a_revision__year=2020,fecha_a_revision__month=5)
    #     ,date_insert__year=2019,date_insert__month=5).order_by('titulo')[:10]

    en_revision = Q(
        id__in=Publicaciones_revision.objects.all().values('publicacion_id'))
    canceladas = Q(
        id__in=Publicaciones_canceladas.objects.all().values('publicacion_id'))
    publicaciones_datos = Publicaciones.objects.select_related('id_cliente').exclude(
        en_revision | canceladas | Q(revisada=1)).only('id_cliente').order_by('id')

    # print(publicaciones_datos.query)
    # datos_pub = Publicaciones_revision.objects.select_related('publicacion').filter(publicacion_id=id_publicacion)

    query = publicaciones_datos.query
    total = Publicaciones.objects.all().count()
    en_revision = Publicaciones_revision.objects.all().count()
    canceladas = Publicaciones_canceladas.objects.all().count()
    por_canceladas = (canceladas / total) * 100
    por_canceladas = f"{canceladas:.2f}"
    pendientes = Publicaciones.objects.filter(
        revisada=0).count() - canceladas - en_revision
    # canceladas = int(canceladas)
    for row in publicaciones_datos:
        row.titulo = reparar_utf8(row.titulo)
    # Show 25 contacts per page.
    paginator = Paginator(publicaciones_datos, 51)

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    datos = {
    "total": total,
    "en_revision": en_revision,
    "canceladas": canceladas,
    "pendientes": pendientes,
    "publicaciones": page_obj,
    "query": query}
    return render(request, "publicaciones.html", datos)


@login_required(login_url='customLogin')
@permitir_secciones_usuarios(grupos_permitidos=['Administrador','Supervisor','Vendedor'])
def publicacion_detallada(request, id_publicacion):

    try:
        datos_pub = Publicaciones.objects.select_related(
            'id_cliente').get(id=id_publicacion)

        datos_cli = {"id": datos_pub.id_cliente.id,
        "nombre": datos_pub.id_cliente.nombre,
        "filetype": datos_pub.id_cliente.filetype}

        # en_algun_proceso = False
        # datos_pub = Publicaciones_revision.objects.select_related('publicacion').filter(publicacion_id=id_publicacion)
        datos_pub.descripcion = reparar_utf8(datos_pub.descripcion)
        existe_canceladas = Publicaciones_canceladas.objects.filter(
            publicacion_id=id_publicacion).exists()
        existe_revision = Publicaciones_revision.objects.filter(
            publicacion_id=id_publicacion).exists()

        if existe_canceladas:
            datos_pub.estado_revisada = "Cancelada"
            # en_algun_proceso = True

        if existe_revision:
            datos_pub.estado_revisada = "En revisión"
            # en_algun_proceso = True

        if datos_pub.revisada == 1:
            datos_pub.estado_revisada = "Aceptada"

        # if datos_pub.revisada  == 0:
        #     datos_pub.estado_revisada =  "Sin revisar"

        dias = {"L": datos_pub.lunes,
        "M": datos_pub.martes,
        "M": datos_pub.miercoles,
        "J": datos_pub.jueves,
        "V": datos_pub.viernes,
        "S": datos_pub.sabado,
        "D": datos_pub.domingo}
        en_algun_proceso = False
        if  existe_canceladas:
            en_algun_proceso = True


        if Publicaciones.objects.filter(id=id_publicacion, revisada=1).exists():
            en_algun_proceso = True

        datos = {"existe": True, "publicacion": datos_pub,
            "cliente": datos_cli, "dias": dias, "proceso": en_algun_proceso}

    except Publicaciones.DoesNotExist:
        datos = {"existe": False, "error": "NOT FOUND"}

    return render(request, "publicacion.html", datos)


@login_required(login_url='customLogin')
@permitir_secciones_usuarios(grupos_permitidos=['Administrador','Supervisor'])
def publicaciones_revision(request):

    fecha_actual = datetime.datetime.now(tz=timezone.utc)
    # revision  = Publicaciones_revision.objects.filter(fecha_limite__date__range=(fecha_inicio, fecha_final))
    revision = Publicaciones_revision.objects.filter(
        fecha_limite__date=fecha_actual) | Publicaciones_revision.objects.filter(fecha_limite__date__gt=fecha_actual)
    recuperados = []
    cursor = connection.cursor()
    sql = """
    select pub.id
        ,pub.id_cliente
          ,pub.titulo,
          pub.descripcion
          ,pub.date_insert
         ,pub.filetype
         ,rev.explicacion
         ,rev.usuario
         ,rev.fecha_a_revision
         ,rev.fecha_limite
         , cli.nombre from publicaciones pub
        inner join publicaciones_revision rev
        on pub.id =  rev.publicacion_id
        inner join clientes cli
        on cli.id = pub.id_cliente
        where exists (select * from publicaciones_revision where rev.publicacion_id = pub.id and  rev.fecha_limite >= CURDATE() )
    """
    cursor.execute(sql)
    publicaciones = cursor.fetchall()
    for pub in publicaciones:
        diccionario = {}
        diccionario = {'id_publicacion': pub[0],
        "id_cliente": pub[1],
        "titulo": pub[2],
        "descripcion": pub[3],
        "date_insert": pub[4],
        "filetype": pub[5],
        "explicacion": pub[6],
        "usuario": pub[7],
        "fecha_a_revision": pub[8],
        "fecha_limite": pub[9].strftime("%d-%b-%Y"),
        "cliente": pub[10],
        "estatus": "En revisión"
        }

        recuperados.append(diccionario)

    # datos_ori_pub = Publicaciones.objects.filter(id__in= Publicaciones_revision.objects.filter(fecha_limite__date = fecha_actual) | Publicaciones_revision.objects.filter(fecha_limite__date__gt=fecha_actual))

    datos = {"pub_revision": revision, "publicaciones": recuperados,
        "cantidad": type(publicaciones)}
    return render(request, "revision.html", datos)



def enviar_Revision(request):
    if request.method == "POST":
        incidencia  = str(random.randint(1, 1000))+ ""+str(random.randint(1,1000))+""+str(random.randint(1,1000))
        respuesta = {}
        id_publicacion = request.POST['id']
        explicacion = request.POST["explicacion"]
        fecha_actual = datetime.datetime.now(tz=timezone.utc)
        fecha_limite = fecha_actual + timedelta(days=7)
        id_cliente = ""
        datos_pub = ""
        try:
            datos_pub = Publicaciones.objects.select_related(
                'id_cliente').get(id=id_publicacion)
            id_cliente = datos_pub.id_cliente.id
        except:
            respuesta["status"] = "error"
            respuesta["message"] = "El id de publicacion no existe"
            return HttpResponse(json.dumps(respuesta))
        
        publicacion_en_revision =  Publicaciones_revision.objects.filter(publicacion_id = id_publicacion)
        if publicacion_en_revision:
            publicacion_revision  = Publicaciones_revision.objects.get(publicacion_id=id_publicacion)
            borrado = publicacion_revision.delete()
            if borrado == False:
                respuesta["status"] = "error"
                respuesta["message"] = "No logro enviarse a revision. Intente mas tarde."
                return HttpResponse(json.dumps(respuesta))

        created = Publicaciones_revision.objects.create(
            publicacion_id=id_publicacion,
            id_cliente=id_cliente, 
            explicacion=explicacion,
            usuario=10,
            fecha_a_revision=fecha_actual,
            fecha_limite=fecha_limite,
            id_usuario=request.session['id'],
            num_incidencia= incidencia
            )
        created.save()
        if created:
            datos_cliente = Clientes.objects.filter(id=id_cliente).first()
            fecha_publicacion  =  datos_pub.date_insert
            cliente  =  reparar_utf8(datos_cliente.nombre)
            titulo_publicacion  =  reparar_utf8(datos_pub.titulo)
            descripcion_publicacion = reparar_utf8(datos_pub.descripcion)
            explicacion =  reparar_utf8(explicacion)
            content_html = """
            <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
</head>


<style>
    .content {
        margin: auto;
        border: 1px red solid;
        width: 65%;
        text-align: justify;
        font: caption;
    }

    /* .logo {
        border: 1px gainsboro solid;
        width: 300px;
        margin: auto;
        margin-top: 10px;
        margin-bottom: -25px;
    }

    @media (max-width: 990px) {
        .content {
            width: 100%;
        }

        .logo {
            /* border: 1px blue solid; */

        /* } */ 
    /* } */
</style>

<body>
    <div class="content" style="font-family: Helvetica, Tahoma, Helvetica, sans-serif;">
        <figure style="width: 250px;
        margin: auto;
        margin-top: 10px;
        margin-bottom: -25px;">
            <img style="width: 100%;" src="""+config.HEADER_IMG+""" alt="">
        </figure>
        <p>&nbsp;</p>
        <h2 style="text-align: center;">Publicación en revisión </h2>
        <h4  style="text-align: center;">Estimado cliente """+cliente+"""</h4>

        <p style="text-align: center;margin-top: 1rem !important;color:black !important;">Pikit comprometidos con sus clientes,usuarios y con una informacion transparente le informamos.</p>
        <p style="text-align:center;color:black !important;">La publicacion <strong>"""+titulo_publicacion+"""</strong>  incumple con nuestras normas en la plataforma  <a
            href="pikit.mx">pikit.mx</a></p>
        <table style="width: 100%; margin-bottom: 1rem; color: #212529;border-collapse:collapse;border: 1xp red solid;">
            <thead style="color: #fff;background-color: #343a40;border: 1px#454d55 solid;">
                <tr >
                    <th style="padding: 10px;" >Concepto</th>
                    <th style="padding: 10px; text-align: justify;border-right: 1px gray solid;">Descripción</th>
                </tr>
            </thead>
            <tbody>
                <tr > 
                    <th style="padding: 10px;border-right: 1px gray solid;border-left: 1px gray solid;" scope="row">Numero incidencia</th>
                    <td style="padding: 10px; text-align: justify;border-right: 1px gray solid;">"""+incidencia+"""</td>

                </tr>
                <tr > 
                    <th style="padding: 10px;border-right: 1px gray solid;border-left: 1px gray solid;" scope="row">Estatus</th>
                    <td style="padding: 10px; text-align: justify;border-right: 1px gray solid;">En revisión</td>

                </tr>
                <tr>
                    <th style="padding: 10px;border-right: 1px gray solid;border-left: 1px gray solid;"  scope="row">Publicaci&oacute;n </th>
                    <td style="padding: 10px; text-align: justify;border-right: 1px gray solid;">"""+titulo_publicacion+"""</td>
                </tr>

                <tr>
                    <th  style="padding: 10px;border-right: 1px gray solid;border-left: 1px gray solid;" scope="row">Fecha publicaci&oacute;n</th>
                    <td style="padding: 10px; text-align: justify;border-right: 1px gray solid;">"""+fecha_publicacion.strftime("%d-%B-%Y")+""" </td>
                </tr>
                <tr>
                    <th  style="padding: 10px;border-right: 1px gray solid;border-left: 1px gray solid;" scope="row">Descripcion publicaci&oacute;n</th>
                    <td style="padding: 10px; text-align: justify;border-right: 1px gray solid;">"""+descripcion_publicacion+"""</td>
                </tr>

                <tr>
                    <th style="padding: 10px;border-right: 1px gray solid;border-left: 1px gray solid;"  scope="row">Motivo de revisión</th>
                    <td style="padding: 10px; text-align: justify;border-right: 1px gray solid;">
                      """+explicacion+"""
                    </td>
                </tr>
                <tr>
                    <th style="padding: 10px;border-right: 1px gray solid;border-bottom: 1px gray solid;border-left: 1px gray solid;"  scope="row">Fecha limite</th>
                    <td style="padding: 10px; text-align: justify;border-right: 1px gray solid;border-bottom: 1px gray solid;">
                        Usted tendra para modificar su publicacion hasta el dia <span
                            style="color: red;"><strong>"""+fecha_limite.strftime("%d-%B-%Y")+"""</strong></span>. Lo
                        podra hacer desde su panel de cliente pikit.
                    </td>
                </tr>

            </tbody>
        </table>



        <p  style="text-align: center;color:black !important">Para mas informacion , dudas o aclaraciones puede comunicarse al correo</p>
        <div style="text-align: center;color:black !important">
        <a class="text-center" href="mailto:elcorreoquequieres@correo.com">"""+config.CORREO_INFO+"""</a>

        </div>
    </div>
</body>

</html>

                """
            destinatario = ""
            if config.ENVIRONMENT:
                destinatario =  datos_cliente.email
            else:
                destinatario = "la.mejia.poot@gmail.com"
                
            
            subject = "Aviso de revisión de publicación "+titulo_publicacion
            enviar_correo(content_html, destinatario,subject)
            respuesta["status"] = "success"
            respuesta["message"] = "Publicación enviada a revisión"
        else:
            respuesta["status"] = "error"
            respuesta["message"] = "Error al enviar a revisión. Intentar mas tarde"

        return HttpResponse(json.dumps(respuesta))

            # print(values)

    else:
        respuesta = {"status": "error", "message": "Bad request1"}
        return HttpResponse(json.dumps(respuesta))


def enviar_cancelacion(request):
    if request.method == "POST":
        id_publicacion = request.POST["id"]
        explicacion = request.POST["explicacion"]
        fecha_actual = datetime.datetime.now(tz=timezone.utc)
        id_cliente = ""
        respuesta = {}
        try:
            datos_pub = Publicaciones.objects.select_related(
                'id_cliente').get(id=id_publicacion)
            id_cliente = datos_pub.id_cliente.id
        except:

            respuesta["status"] = "error"
            respuesta["message"] = "El id de publicacion no existe"
            return HttpResponse(json.dumps(respuesta))

        try:
            Publicaciones_canceladas.objects.get(publicacion=id_publicacion)

            respuesta["status"] = "error"
            respuesta["message"] = "Esta publicacion ya fue cancelada anteriormente"
        except Publicaciones_canceladas.DoesNotExist:
            existe_en_revision  =  Publicaciones_revision.objects.filter(publicacion_id = id_publicacion).exists()
            if existe_en_revision:
                borrado_de_revision  = Publicaciones_revision.objects.get(publicacion_id=id_publicacion).delete()
                if borrado_de_revision == False:
                    respuesta["status"] = "error"
                    respuesta["message"] =  "No se logro cancelar la publicacion. Intente mas tarde"
                    return HttpResponse(json.dumps(respuesta))

            obj, created = Publicaciones_canceladas.objects.filter(
            Q(publicacion_id=id_publicacion),).get_or_create(
            publicacion_id=id_publicacion,
            id_cliente=id_cliente,
            explicacion=explicacion,
            usuario=request.session['id'],
            fecha_cancelada=fecha_actual,
            id_usuario=request.session['id']
            )
            
            if created:
                titulo_publicacion =  reparar_utf8( datos_pub.titulo)
                datos_cliente = Clientes.objects.filter(id=id_cliente).first()
                cliente  = reparar_utf8( datos_cliente.nombre)
                descripcion_publicacion = reparar_utf8(datos_pub.descripcion)
                explicacion =  reparar_utf8(explicacion)
                fecha_publicacion = datos_pub.date_insert

                content_html = """
            <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>


<style>
    .content {
        margin: auto;
        border: 1px red solid;
        width: 65%;
        text-align: justify;
        font: caption;
    }

    /* .logo {
        border: 1px gainsboro solid;
        width: 300px;
        margin: auto;
        margin-top: 10px;
        margin-bottom: -25px;
    }

    @media (max-width: 990px) {
        .content {
            width: 100%;
        }

        .logo {
            /* border: 1px blue solid; */

        /* } */ 
    /* } */
</style>

<body>
    <div class="content" style="font-family: Helvetica, Tahoma, Helvetica, sans-serif;">
        <figure style="width: 250px;
        margin: auto;
        margin-top: 10px;
        margin-bottom: -25px;">
            <img style="width: 100%;" src="""+config.HEADER_IMG+""" alt="">
        </figure>
        <p>&nbsp;</p>
        <h2 style="text-align: center; color:red">Publicación cancelada </h2>
        <h4  style="text-align: center;">Estimado cliente """+cliente+"""</h4>

        <p style="text-align: center;margin-top: 1rem !important;color:black !important;">Pikit comprometidos con sus clientes,usuarios y con una informacion transparente le informamos.</p>
        <p style="text-align:center;color:black !important;">Su publicacion <strong>"""+titulo_publicacion+"""</strong> ha sido cancelada ya que   incumple con nuestras normas en la plataforma  <a
            href="pikit.mx">pikit.mx</a></p>
        <table style="width: 100%; margin-bottom: 1rem; color: #212529;border-collapse:collapse;border: 1xp red solid;">
            <thead style="color: #fff;background-color: #343a40;border: 1px#454d55 solid;">
                <tr >
                    <th style="padding: 10px;" >Concepto</th>
                    <th style="padding: 10px; text-align: justify;border-right: 1px gray solid;">Descripción</th>
                </tr>
            </thead>
            <tbody>
                <tr > 
                    <th style="padding: 10px;border-right: 1px gray solid;border-left: 1px gray solid;" scope="row">Estatus</th>
                    <td style="padding: 10px; text-align: justify;border-right: 1px gray solid;color:red">Cancelada</td>

                </tr>
                <tr>
                    <th style="padding: 10px;border-right: 1px gray solid;border-left: 1px gray solid;"  scope="row">Publicaci&oacute;n </th>
                    <td style="padding: 10px; text-align: justify;border-right: 1px gray solid;">"""+titulo_publicacion+"""</td>
                </tr>

                <tr>
                    <th  style="padding: 10px;border-right: 1px gray solid;border-left: 1px gray solid;" scope="row">Fecha publicaci&oacute;n</th>
                    <td style="padding: 10px; text-align: justify;border-right: 1px gray solid;">"""+fecha_publicacion.strftime("%d-%B-%Y")+""" </td>
                </tr>
                <tr>
                    <th  style="padding: 10px;border-right: 1px gray solid;border-left: 1px gray solid;" scope="row">Descripcion publicaci&oacute;n</th>
                    <td style="padding: 10px; text-align: justify;border-right: 1px gray solid;">"""+descripcion_publicacion+"""</td>
                </tr>

                <tr>
                    <th style="padding: 10px;border-right: 1px gray solid;border-left: 1px gray solid;"  scope="row">Motivo de cancelación</th>
                    <td style="padding: 10px; text-align: justify;border-right: 1px gray solid;">
                      """+explicacion+"""
                    </td>
                </tr>
                <tr>
                    <th style="padding: 10px;border-right: 1px gray solid;border-bottom: 1px gray solid;border-left: 1px gray solid;"  scope="row">Fecha de cancelación</th>
                    <td style="padding: 10px; text-align: justify;border-right: 1px gray solid;border-bottom: 1px gray solid;">
                         <span
                            style="color: red;"><strong>"""+fecha_actual.strftime("%d-%B-%Y")+"""</strong></span>
                    </td>
                </tr>

            </tbody>
        </table>



        <p  style="text-align: center;color:black !important">Para mas informacion , dudas o aclaraciones puede comunicarse al correo</p>
        <div style="text-align: center;color:black !important">
        <a class="text-center" href="mailto:elcorreoquequieres@correo.com">"""+config.CORREO_INFO+"""</a>

        </div>
    </div>
</body>

</html>
                """
                if config.ENVIRONMENT:
                    destinatario =  datos_cliente.email
                else:
                    destinatario = "la.mejia.poot@gmail.com"

                subject = "Aviso de cancelacion de publicación "+titulo_publicacion
                enviar_correo(content_html, destinatario,subject)
                respuesta["status"] = "success"
                respuesta["message"] = "Publicación cancelada "
            else:
                respuesta["status"] = "error"
                respuesta["message"] = "Error al cancelar la publicación"
        return HttpResponse(json.dumps(respuesta))

    else:
        respuesta = {"status": "error", "message": "Bad request"}
        return HttpResponse(json.dumps(respuesta))


@login_required(login_url='customLogin')
@permitir_secciones_usuarios(grupos_permitidos=['Administrador','Supervisor'])
def canceladas(request):

    fecha_actual = datetime.datetime.now(tz=timezone.utc)
    # revision  = Publicaciones_revision.objects.filter(fecha_limite__date__range=(fecha_inicio, fecha_final))
    # revision  = Publicaciones_revision.objects.filter(fecha_limite__date = fecha_actual) | Publicaciones_revision.objects.filter(fecha_limite__date__gt=fecha_actual)
    recuperados = []
    cursor = connection.cursor()
    sql = """
    select pub.id
        ,pub.id_cliente
          ,pub.titulo,
          pub.descripcion
          ,pub.date_insert
         ,pub.filetype
         ,can.explicacion
         ,us.username
         ,can.fecha_cancelada
         , cli.nombre from publicaciones pub
        inner join publicaciones_canceladas can
        on pub.id =  can.publicacion_id
        inner join clientes cli
        on cli.id = pub.id_cliente
        inner join auth_user us
        on us.id =  can.id_usuario
        where exists (select * from publicaciones_revision where can.publicacion_id = pub.id)
    """
    cursor.execute(sql)
    publicaciones = cursor.fetchall()
    for pub in publicaciones:
        diccionario = {}
        diccionario = {'id_publicacion': pub[0],
        "id_cliente": pub[1],
        "titulo": pub[2],
        "descripcion": pub[3],
        "date_insert": pub[4],
        "filetype": pub[5],
        "explicacion": pub[6],
        "usuario": pub[7],
        "fecha_cancelacion": pub[8].strftime("%d-%b-%Y"),
        "cliente": pub[9],
        "estatus": "Cancelada"
        }

        recuperados.append(diccionario)

    # datos_ori_pub = Publicaciones.objects.filter(id__in= Publicaciones_revision.objects.filter(fecha_limite__date = fecha_actual) | Publicaciones_revision.objects.filter(fecha_limite__date__gt=fecha_actual))

    datos = {"publicaciones": recuperados}
    return render(request, "canceladas.html", datos)


# @ajax_required
def aceptar_publicaciones(request):
    if request.method == 'POST':
        id_publicacion = request.POST["id"]
        respuesta = {}

        try:
            publicacion = Publicaciones.objects.get(id=id_publicacion)

            try:
                publicacion_en_revision =  Publicaciones_revision.objects.get(publicacion_id=id_publicacion)
                publicacion_en_revision.delete()
            except Publicaciones_revision.DoesNotExist:
                pass

            try:
                publicacion_en_cancelada =  Publicaciones_canceladas.objects.get(publicacion_id=id_publicacion)
                publicacion_en_cancelada.delete()
            except Publicaciones_canceladas.DoesNotExist:
                pass
            publicacion.revisada = 1
            actualizado = publicacion.save()
            respuesta["status"] = "success"
            respuesta["message"] = "Publicación aprobada"


        except Publicaciones.DoesNotExist:
            respuesta["status"] = "error"
            respuesta["message"] = "La publicacion no  existe "

        return HttpResponse(json.dumps(respuesta))

    else:
        respuesta = {"status": "error", "message": "Bad request"}
        return HttpResponse(json.dumps(respuesta))


@login_required(login_url='customLogin')
@permitir_secciones_usuarios(grupos_permitidos=['Administrador','Supervisor'])
def publicaciones_aceptadas(request):

    aprobadas = Publicaciones.objects.filter(revisada=1)
    for row in aprobadas:
        try:
            row.titulo = row.titulo.encode('latin1').decode('utf8')
        except:
            row.titulo = row.titulo
    datos = {"aprobadas": aprobadas}
    return render(request, "aprobadas.html", datos)


def clientes(request):
    clientes = Clientes.objects.filter(category_id=2)
    datos = {"clientes": clientes}
    return render(request, "clientes.html", datos)


def buscar_publicaciones(request):
    if not request.is_ajax():
        datos = {"status":"error","message":"Deber ser una peticion Ajax"}
        return HttpResponseBadRequest(json.dumps(datos))

    if request.method == "POST":
        palabra =  reparar_utf8(request.POST['palabra'])
        tipo = request.POST['tipo_busqueda']
        print(tipo)

        en_revision = Q(id__in=Publicaciones_revision.objects.all().values('publicacion_id'))
        canceladas = Q(id__in=Publicaciones_canceladas.objects.all().values('publicacion_id'))
        publicaciones_datos = ""
        
        if tipo == "cliente":

            clientes =  Clientes.objects.filter(nombre__icontains=palabra).values('id')
            publicaciones_datos = Publicaciones.objects.select_related('id_cliente').filter(id_cliente__in=clientes).exclude(
            en_revision | canceladas | Q(revisada=1) ).order_by('id')[:30]


        if tipo == "titulo":

            publicaciones_datos = Publicaciones.objects.select_related('id_cliente').filter(titulo__icontains=palabra).exclude(
            en_revision | canceladas | Q(revisada=1) ).order_by('id')[:30]
        if tipo == "descripcion":

            publicaciones_datos = Publicaciones.objects.select_related('id_cliente').filter(descripcion__icontains=palabra).exclude(
            en_revision | canceladas | Q(revisada=1) ).order_by('id')[:30]

        if tipo =="NA":
            
            publicaciones_datos = Publicaciones.objects.select_related('id_cliente').filter(Q(descripcion__icontains=palabra) | Q(titulo__icontains=palabra)).exclude(
            en_revision | canceladas | Q(revisada=1) ).order_by('id')[:30]
        respuesta  = []
        for publicacion in publicaciones_datos:
            publi  = {}
            publi = {
                "id_publicacion": publicacion.id,
                "filetype_publicacion":publicacion.filetype,
                "cliente":publicacion.id_cliente.nombre,
                "id_cliente":publicacion.id_cliente.id,
                "filetype_cliente":publicacion.id_cliente.filetype,
                "titulo":reparar_utf8(publicacion.titulo[0:17]),
                "descripcion":reparar_utf8(publicacion.descripcion),
                "fecha":publicacion.date_insert.strftime("%d-%b-%Y")
            }
            respuesta.append(publi)
        if len(respuesta) == 0:
            datos = {"status":"error","message":"No se encontraron publicaciones :("}
            return HttpResponse(json.dumps(datos))
        
        datos = {"status":"success","publicaciones":respuesta}
        return HttpResponse(json.dumps(datos))
            
            

    else:
        datos = {"status":"error", "message":"Bad request"}
        return HttpResponseBadRequest(json.dumps(datos))



def enviar_correo(content_html, destinatario,subject):

    msg = MIMEMultipart()
    password = config.PASS_MAIL
    msg['From'] =config.USER_MAIL
    # msg['To'] =", ".join(destinatarios)
    msg['To'] = destinatario
    msg['Subject'] = subject
    msg.attach(MIMEText(content_html, 'html'))
    server = smtplib.SMTP(config.SERVIDOR_MAIL,587)
    server.ehlo()
    server.starttls()
    server.ehlo()
    server.login(msg['From'],password)
    enviado = server.sendmail(msg['From'],msg['To'],msg.as_string())
    server.quit()
    
