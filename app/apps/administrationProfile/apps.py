from django.apps import AppConfig


class AdministrationprofileConfig(AppConfig):
    name = 'administrationProfile'
