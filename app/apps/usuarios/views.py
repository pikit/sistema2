from django.shortcuts import render,redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.auth import login , logout
from django.http import HttpResponse,JsonResponse
import json
import re
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.contrib.auth.models import Group
from generic.decorators import * 


from usuarios.models import *
from vendedores.models import  * 
# Create your views here.

@login_required(login_url='customLogin')
@permitir_secciones_usuarios(grupos_permitidos=['Administrador','Supervisor'])
def signup(request):
    tipos_usuarios =  AuthGroup.objects.all().order_by('id')
    mensaje = ""
    respuesta = {}
    if request.method =='POST':
        datos = request.POST.getlist('datos[]')
        usuario  =  datos[1]
        nombre =  datos[2]
        apellidos =  datos[3]
        correo  =  datos[4]
        password1 =  datos[5]
        password2 =  datos[6]
        tipo_usuario = datos[7]
        if request.user.is_authenticated == False:
            respuesta = {'status':"error", "message":"necesitas estar logeado para agregar un usuario"}
            return HttpResponse(json.dumps(respuesta))
        if password1 != password2:
            respuesta = {"status":"error","message":"Las contraseñas son diferentes"}
            return HttpResponse(json.dumps(respuesta))
        
        if User.objects.filter(email=correo).exists():
            respuesta = {"status":"error","message":"El correo ya existe"}
            return HttpResponse(json.dumps(respuesta))
        if User.objects.filter(username=usuario).exists():
            respuesta = {"status":"error","message":"El usuario ya existe"}
            return HttpResponse(json.dumps(respuesta))

        admin =  False
        supervisor = False
        gerente = False
        vendedor =  False
        roles  = []
        for grupo in tipos_usuarios:
            if grupo.id == int(tipo_usuario):
                roles.append(1)
            else:
                roles.append(0)
    
        # form =  UserCreationFormCustom(request.POST)
        # if User.objects.filter(email=request.POST["email"]).exists():
        #     # return redirect('en_revision')
        #     datos = {"tipos_usuarios":tipos_usuarios, "mensaje":"El correo ya es usado por otro usuario"}
        #     return render(request, "signup.html",datos)
        creado  = User.objects.create_user(
            username=usuario, 
            email=correo,
            password=password1,
            first_name =  nombre,
            last_name =  apellidos,
            is_staff=False,
            is_administrador = roles[0],
            is_supervisor = roles[1],
            is_gerente_ventas = roles[2],
            is_vendedor = roles[3]
            
             )
        # id_creado = creado.id
        # act =  AuthUser.objects.get(id=id_creado)
        # act.is_staff = tipo_usuario
        # act.save()
        id_creado  =  creado.id
        grupo  =  Group.objects.get(id = tipo_usuario)    
        usuario_creado = User.objects.get(id=id_creado)
        asignado_grupo  = usuario_creado.groups.add(grupo)

        si_gerente = 2
        vendedor = False
        tipo_usuario = int(tipo_usuario)
        #gerente de ventas
        if tipo_usuario == 3:
            si_gerente = 1
            vendedor = True
        #vendedor 
        elif tipo_usuario == 4 :
            si_gerente = 0
            vendedor = True
        # no aplica para la seccion de vendedores
        else:
            si_gerente = 2
            vendedor= False
        crear = ""
        if  vendedor:
            crear =  Vendedores.objects.create(
            is_gerente = si_gerente,
            is_activo  = True,
            usuario_id = id_creado)


        if id_creado > 0 :
            # datos = {"status":tipos_usuarios, "mensaje":"Usuario guardado exitosamente"}
            respuesta = {"status":"success","message":"Usuario creado exitosamente"}
            return HttpResponse(json.dumps(respuesta))
            # return render(request, "signup.html",datos)
            # return redirect('publicaciones')
        
            

    else: 

        datos = {"tipos_usuarios":tipos_usuarios,"mensaje":""}
        return render(request, "signup.html",datos)

    # datos = {"form":form,"tipos_usuarios":tipos_usuarios}
    # return render(request, "signup.html",datos)
    # else:
    #     pass

@login_required(login_url="customLogin")
@permitir_secciones_usuarios(grupos_permitidos=['Administrador','Supervisor','GerenteVentas'])
def user_data(request,id_usuario):
    datos = {}
    grupos =  AuthGroup.objects.all().order_by('id')
    grupo =""
    try:
        usuario  =  AuthUser.objects.get(id=id_usuario)
        estatus  = {"Activo":1,"Desactivado":0}

        # grupos_asignados =  AuthUserGroups.objects.select_related('group').get(user_id=id_usuario)
        try:
            grupos_asignados =  AuthUserGroups.objects.select_related('group').get(user_id=id_usuario)
            grupo =  grupos_asignados.group.id
        except AuthUserGroups.DoesNotExist:
            # al no tener uno asignado se le asigna el rango mas bajo
            grupo = 4 

        vendedor = False
        tipo_vendedor = 0
        if Vendedores.objects.filter(usuario_id  =  id_usuario).exists():
            vend = Vendedores.objects.get(usuario_id = id_usuario)
            if vend.is_gerente:
                tipo_vendedor  = 1 #gerente
            else:
                tipo_vendedor = 0 #vendedor

            vendedor =  True
        datos = {"existe":True,"usuario":usuario,"tipos_usuarios":grupos,"estatus":estatus
        ,"grupo":grupo,
        "vendedor": vendedor,
        "tipo_vendedor":tipo_vendedor}
    except AuthUser.DoesNotExist:
        datos  = {"existe":False}
    

    return render(request,"update.html",datos)
    # tipos_usuarios =  UsuariosPortalAdmin.objects.all()
    # if request.method == 'POST':
    #     pass
    # else:
    #     datos = {"tipos_usuarios":tipos_usuarios}
    #     return render(request,'update.html',datos)


@permitir_secciones_usuarios(grupos_permitidos=['Administrador','Supervisor'])
def update_user(request):
    if request.method ==  'POST':
        datos = request.POST.getlist('datos[]')
        tipos_usuarios =  AuthGroup.objects.all().order_by('id')
        
        usuario =  datos[1]
        tipo_usuario =  datos[2]
        nombre =  datos[3]
        apellidos = datos[4]
        correo  =  datos[5]
        estatus =  datos[6]
        estatus_vendedor  =  datos[7]
        id_usuario  = datos[8]

        roles  = []
        for grupo in tipos_usuarios:
            if grupo.id == int(tipo_usuario):
                roles.append(1)
            else:
                roles.append(0)
        # correo = ""
        # print(roles)
        if  request.user.is_authenticated == False:
            datos =  {"status":"error","message":"Debes de iniciar sesion primero"}
            return HttpResponse(json.dumps(datos))
        correo =  correo.strip()
        # return HttpResponse(user.query)
        valido =  validar_correo(correo)
        if valido == False:
            datos = {"status":"info","message":"Formato incorrecto de correo electronico"}
            return HttpResponse(json.dumps(datos))
        
        user  = AuthUser.objects.filter(~Q(id=id_usuario),email=correo)
        # return HttpResponse(user.query)
        if user:

            datos =  {"status":"info","message":"El correo ya esta uso por otro usuario"}
            return HttpResponse(json.dumps(datos))

        try:
            usuario  =  AuthUser.objects.get(id=id_usuario)
            # usuario.is_staff =  tipo_usuario
            usuario.first_name =  nombre
            usuario.last_name =  apellidos
            usuario.email =  correo
            usuario.is_active =  estatus
            usuario.save()

            # print(roles)
            
            edit_grupo =  User.objects.get(id=id_usuario)
            edit_grupo.is_administrador = roles[0]
            edit_grupo.is_supervisor = roles[1]
            edit_grupo.is_gerente_ventas = roles[2]
            edit_grupo.is_vendedor = roles[3]
            edit_grupo.save()

            grupo_nuevo  =  tipo_usuario
            if AuthUserGroups.objects.filter(user_id=id_usuario).exists():
                grupo =  AuthUserGroups.objects.get(user_id=id_usuario)
                grupo.group_id = grupo_nuevo
                grupo.save()
            else:
                grupo  =  Group.objects.get(id = grupo_nuevo)    
                usuario_creado = User.objects.get(id=id_usuario)
                asignado_grupo  = usuario_creado.groups.add(grupo)

            #gerente
            tipo_usuario = int(tipo_usuario)
            print(tipo_usuario)
            if tipo_usuario == 3:
                estatus_vendedor = 1
            # vendedor
            elif tipo_usuario == 4:
                estatus_vendedor = 0
            # no es vendedor
            else:
                estatus_vendedor = 2
            update_vendedor(id_usuario, estatus_vendedor)

            datos = {"status":"success","message":"Usuario Modificado"}
            return HttpResponse(json.dumps(datos))
        except AuthUser.DoesNotExist:
            datos =  {"status":"error","message":"El usuario no existe"}
            return HttpResponse(json.dumps(datos))
        
    else:
        datos = {"status":"error","message":"Error method"}
        return HttpResponse(json.dumps(datos))

def user_login(request):

    if request.method  ==  "POST":
        credenciales = {}
        user = ""
        activado  = ""
        if validar_correo(request.POST["username"]):
            try:
                user  =  AuthUser.objects.get(email=request.POST["username"])
                activado  =  user.is_active
                user = user.username
            except AuthUser.DoesNotExist:
                return render(request, "login.html",{"error":"El correo no existe"}) 
        else:
            try:
                user  =  AuthUser.objects.get(username=request.POST["username"])
                activado  =  user.is_active
                user =  user.username
            except AuthUser.DoesNotExist:
                return render(request, "login.html",{"error":"El  usuario no existe"})
        if activado  == 0 : 
            return render(request, "login.html",{"error":"Usuario desactivado"})
        credenciales["username"] = user
        credenciales["password"] = request.POST["password"]
        form  =  AuthenticationForm(data=credenciales)
        if form.is_valid():
            user =  form.get_user()
            login(request,user)
            request.session["usuario"] = request.POST["username"]
            usuario  =  AuthUser.objects.get(username=user)
            request.session["nombre"] =  usuario.username
            # usuario.first_name+" "+usuario.last_name
            request.session['id'] =  usuario.id
            roles  = User.objects.get(id=usuario.id)
            request.session['is_administrador'] = roles.is_administrador
            request.session['is_supervisor'] =  roles.is_supervisor
            request.session['is_gerente_ventas'] =  roles.is_gerente_ventas
            request.session['is_vendedor'] = roles.is_vendedor
            # request.session[""]
            if 'next' in request.POST:
                return  redirect(request.POST.get('next','customLogin'))
            else:
                return redirect('pagina_principal')
        else:
            return render(request, "login.html",{"error":"Datos inválidos"})
            

    else:
        form  =  AuthenticationForm()
        return render(request, "login.html",{"form":form})



def logout(request):
    if request.method == "POST":
        logout(request)
        return redirect("/")
    else:
        pass
    # else:
@login_required(login_url='customLogin')
@permitir_secciones_usuarios(grupos_permitidos=['Administrador','GerenteVentas','Vendedor'])
def listado_usuarios(request):
    grupos  =  AuthGroup.objects.all().order_by('id')
    usuarios =  User.objects.all().order_by('id')
    # usuarios = AuthUserGroups.objects.select_related('group').all().order_by('id')
     
    datos  = {"usuarios": usuarios, "grupos": grupos}
    return render(request,"listado.html",datos)



def cambiar_estatus_usuario(request):
    if request.method == "POST":
        if request.user.is_authenticated == False:
            respuesta = {'status':"error", "message":"necesitas estar logeado para agregar un usuario"}
            return HttpResponse(json.dumps(respuesta))

        id_usuario =  request.POST["id"]
        respuesta = {}

        try:
            usuario  =  AuthUser.objects.get(id=id_usuario)

        except AuthUser.DoesNotExist:
            respuesta  = {"status":"error","message":"El usuario no existe"}
            return HttpResponse(json.dumps(respuesta))


        usuario.is_active  = request.POST["estatus"]
        guardado  =  usuario.save()
        mensaje = ""
        if request.POST["estatus"]  == "1":
            mensaje = "El usuario "+usuario.username+" fue  Activado"
        if request.POST["estatus"] == "0":
            mensaje  = "El usuario "+usuario.username+" fue desactivado"
        
        respuesta = {"status":"success","message":mensaje}
        return HttpResponse(json.dumps(respuesta))

        # if guardado:
            
        # else:
        #     respuesta ={"status":"error","message":"No se logro desactivar al usuario"}
        
        
def update_vendedor(id_usuario, estatus):
    estatus  =  int(estatus)
    try:
        vendedor  = Vendedores.objects.get(usuario_id = id_usuario)
        if estatus == 2 :
            vendedor.delete()
            return True
        if estatus  ==  1:
            vendedor.is_gerente = 1
        else:
            vendedor.is_gerente = 0
        vendedor.save()
        return True
            
    except Vendedores.DoesNotExist:
        if estatus  ==  2:
            return True
        crear =  Vendedores.objects.create(
            is_gerente = estatus,
            is_activo  = True,
            usuario_id = id_usuario
        )
        if crear:
            return True
        else:
            return False
        

        

def validar_correo(correo):
    if re.match('^[(a-z0-9\_\-\.)]+@[(a-z0-9\_\-\.)]+\.[(a-z)]{2,15}$',correo.lower()):
        return True
    else:
        return False





    