from django.conf.urls import url
from django.urls import path
from . import views
urlpatterns = [
    # path('cliente/<int:id_cliente>',views.cliente, name="cliente"),
    # path('listado-clientes',views.listado_clientes, name = "listado_clientes"), 

    path('registro-usuarios', views.signup, name="registroUsuarios"),
    path('logout', views.logout, name="logout"),
    path('listado-usuarios', views.listado_usuarios, name="listadoUsuarios"),
    path('desactivar_usuario', views.cambiar_estatus_usuario, name="desactivarUsuario"),
    path('actualizar-usuario/<int:id_usuario>',views.user_data,name="actualizarUsuario"),
    path('metodoActualizar',views.update_user,name="metodoActualizar")

    # path('login', views.login, name="login"),
    # path('prueba', views.login, name="pruebausuarios"),
]