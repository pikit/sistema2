from django.conf.urls import url
from django.urls import path
from . import views
urlpatterns = [
    path('cliente/<int:id_cliente>',views.cliente, name="cliente"),
    path('listado-clientes',views.listado_clientes, name = "listado_clientes"),
    path('publicaciones-cliente/<int:id_cliente>',views.publicaciones_por_cliente, name="publicaciones_por_cliente"), 
    path('get-municipios',views.get_municipios,name = "get_municipios"),
    path('buscar-clientes',views.buscador_clientes,name="buscador_clientes"),
    # path('enviarRevision', EntityAjaxView.as_view(), name="enviar_revision")
]