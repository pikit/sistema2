﻿# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class ApiKeys(models.Model):
    api_key = models.CharField(primary_key=True, max_length=128)
    date_creation = models.DateTimeField()
    date_modification = models.DateTimeField()
    active = models.IntegerField()
    user_id = models.PositiveIntegerField()

    class Meta:
        managed = False
        db_table = 'api_keys'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class Clientes(models.Model):
    category_id = models.IntegerField(blank=True, null=True)
    nombre = models.CharField(max_length=250)
    perfil = models.IntegerField()
    telefono = models.CharField(max_length=50)
    direccion = models.CharField(max_length=250)
    estado = models.IntegerField()
    ciudad = models.IntegerField()
    cp = models.PositiveIntegerField(blank=True, null=True)
    email = models.CharField(max_length=250)
    password = models.CharField(max_length=250)
    clave = models.CharField(max_length=250, blank=True, null=True)
    confirmado = models.IntegerField()
    tipo_inscripcion = models.IntegerField(blank=True, null=True)
    razon_social = models.CharField(max_length=250, blank=True, null=True)
    rfc = models.CharField(max_length=250, blank=True, null=True)
    email_fac = models.CharField(max_length=250, blank=True, null=True)
    calle_fac = models.CharField(max_length=250, blank=True, null=True)
    numero_fac = models.CharField(max_length=250, blank=True, null=True)
    colonia_fac = models.CharField(max_length=250, blank=True, null=True)
    cp_fac = models.IntegerField(blank=True, null=True)
    estado_fac = models.IntegerField(blank=True, null=True)
    ciudad_fac = models.CharField(max_length=250, blank=True, null=True)
    nombre_contacto = models.CharField(max_length=250, blank=True, null=True)
    apellido_contacto = models.CharField(max_length=250, blank=True, null=True)
    telefono_contacto = models.CharField(max_length=250, blank=True, null=True)
    email_contacto = models.CharField(max_length=250, blank=True, null=True)
    filetype = models.CharField(max_length=4, blank=True, null=True)
    update_email = models.CharField(max_length=250, blank=True, null=True)
    idcostumeropenpay = models.CharField(db_column='idCostumerOpenPay', max_length=250, blank=True, null=True)  # Field name made lowercase.
    idtdcopenpay = models.CharField(db_column='idTdcOpenPay', max_length=250, blank=True, null=True)  # Field name made lowercase.
    digitscard = models.IntegerField(db_column='digitsCard', blank=True, null=True)  # Field name made lowercase.
    idsubsopenpay = models.CharField(db_column='idSubsOpenPay', max_length=250, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'clientes'


class ClientesUsuarios(models.Model):
    nombre = models.CharField(max_length=250)
    telefono = models.CharField(max_length=250)
    id_sucursal = models.PositiveIntegerField()
    email = models.CharField(max_length=250)
    password = models.CharField(max_length=250)
    id_cliente = models.PositiveIntegerField()
    clave = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'clientes_usuarios'


class ClientesUsuariosSucursalesRel(models.Model):
    id_usuario = models.PositiveIntegerField()
    id_sucursal = models.PositiveIntegerField()
    activo = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'clientes_usuarios_sucursales_rel'


class ClientsCategories(models.Model):
    name = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'clients_categories'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Estados(models.Model):
    clave = models.CharField(max_length=2)
    name = models.CharField(max_length=45)
    abrev = models.CharField(max_length=16)
    abrev_pm = models.CharField(max_length=16)
    id_country = models.IntegerField(blank=True, null=True)
    activo = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'estados'


class Logsopenpay(models.Model):
    error = models.TextField(blank=True, null=True)
    request = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'logsOpenPay'


class MerchantLocationsImages(models.Model):
    merchant_location_id = models.PositiveIntegerField()
    image_order = models.PositiveIntegerField()
    filetype = models.CharField(max_length=4)

    class Meta:
        managed = False
        db_table = 'merchant_locations_images'


class Municipios(models.Model):
    estado_id = models.IntegerField()
    clave = models.IntegerField()
    name = models.CharField(max_length=50)
    sigla = models.CharField(max_length=4)
    active = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'municipios'


class Publicaciones(models.Model):
    id_cliente = models.PositiveIntegerField()
    titulo = models.CharField(max_length=250)
    descripcion = models.TextField(blank=True, null=True)
    horario_apertura = models.PositiveIntegerField()
    horario_cierre = models.PositiveIntegerField()
    edad_inicio = models.PositiveIntegerField(blank=True, null=True)
    edad_limite = models.PositiveIntegerField(blank=True, null=True)
    lunes = models.PositiveIntegerField(blank=True, null=True)
    martes = models.PositiveIntegerField(blank=True, null=True)
    miercoles = models.PositiveIntegerField(blank=True, null=True)
    jueves = models.PositiveIntegerField(blank=True, null=True)
    viernes = models.PositiveIntegerField(blank=True, null=True)
    sabado = models.PositiveIntegerField(blank=True, null=True)
    domingo = models.PositiveIntegerField(blank=True, null=True)
    hombres = models.PositiveIntegerField(blank=True, null=True)
    mujeres = models.PositiveIntegerField(blank=True, null=True)
    ambos = models.PositiveIntegerField(blank=True, null=True)
    promo_cumple = models.PositiveIntegerField(blank=True, null=True)
    evento = models.IntegerField(blank=True, null=True)
    date_insert = models.DateTimeField()
    filetype = models.CharField(max_length=4, blank=True, null=True)
    revisada = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'publicaciones'


class PublicacionesCanceladas(models.Model):
    id_cliente = models.PositiveIntegerField()
    explicacion = models.CharField(max_length=250)
    usuario = models.PositiveIntegerField()
    fecha_cancelada = models.DateTimeField()
    publicacion_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'publicaciones_canceladas'


class PublicacionesRevision(models.Model):
    publicacion_id = models.IntegerField()
    id_cliente = models.PositiveIntegerField()
    explicacion = models.CharField(max_length=250)
    usuario = models.PositiveIntegerField()
    fecha_a_revision = models.DateTimeField()
    fecha_limite = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'publicaciones_revision'


class PublicationsMerchantLocationsRelationship(models.Model):
    publication_id = models.PositiveIntegerField()
    merchant_location_id = models.PositiveIntegerField()
    active = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'publications_merchant_locations_relationship'


class PublicationsStats(models.Model):
    publication_id = models.IntegerField(blank=True, null=True)
    type = models.IntegerField(blank=True, null=True)
    record_date = models.DateTimeField(blank=True, null=True)
    gender = models.IntegerField(blank=True, null=True)
    user_id = models.IntegerField(blank=True, null=True)
    user_age = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'publications_stats'


class RegisteredUsers(models.Model):
    name = models.CharField(max_length=250, blank=True, null=True)
    email = models.CharField(max_length=250, blank=True, null=True)
    birthdate = models.DateField(blank=True, null=True)
    gender = models.IntegerField(blank=True, null=True)
    pin_code = models.CharField(max_length=250, blank=True, null=True)
    registration_type = models.IntegerField(blank=True, null=True)
    password = models.CharField(max_length=250)
    active = models.PositiveIntegerField()

    class Meta:
        managed = False
        db_table = 'registered_users'


class Sepomex(models.Model):
    idestado = models.PositiveSmallIntegerField(db_column='idEstado')  # Field name made lowercase.
    estado = models.CharField(max_length=35)
    idmunicipio = models.PositiveSmallIntegerField(db_column='idMunicipio')  # Field name made lowercase.
    municipio = models.CharField(max_length=60)
    ciudad = models.CharField(max_length=60, blank=True, null=True)
    zona = models.CharField(max_length=15)
    cp = models.IntegerField()
    asentamiento = models.CharField(max_length=70)
    tipo = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'sepomex'


class Sucursales(models.Model):
    id_cliente = models.IntegerField()
    nombre = models.CharField(max_length=250)
    telefono = models.CharField(max_length=250)
    email = models.CharField(max_length=250, blank=True, null=True)
    website = models.CharField(max_length=250, blank=True, null=True)
    facebook = models.CharField(max_length=250, blank=True, null=True)
    instagram = models.CharField(max_length=250, blank=True, null=True)
    direccion = models.CharField(max_length=250)
    colonia = models.PositiveIntegerField(blank=True, null=True)
    latitud = models.CharField(max_length=250)
    longitud = models.CharField(max_length=250)
    s_domicilio = models.IntegerField(blank=True, null=True)
    s_credito = models.IntegerField(blank=True, null=True)
    s_ninos = models.IntegerField(blank=True, null=True)
    s_fumar = models.IntegerField(blank=True, null=True)
    s_esta = models.IntegerField(blank=True, null=True)
    s_valet = models.IntegerField(blank=True, null=True)
    s_wifi = models.IntegerField(blank=True, null=True)
    s_aire = models.IntegerField(blank=True, null=True)
    lunes = models.IntegerField(blank=True, null=True)
    martes = models.IntegerField(blank=True, null=True)
    miercoles = models.IntegerField(blank=True, null=True)
    jueves = models.IntegerField(blank=True, null=True)
    viernes = models.IntegerField(blank=True, null=True)
    sabado = models.IntegerField(blank=True, null=True)
    domingo = models.IntegerField(blank=True, null=True)
    lunes_inicio = models.IntegerField(blank=True, null=True)
    lunes_final = models.IntegerField(blank=True, null=True)
    lunes_inicio_corte = models.IntegerField(blank=True, null=True)
    lunes_final_corte = models.IntegerField(blank=True, null=True)
    martes_inicio = models.IntegerField(blank=True, null=True)
    martes_final = models.IntegerField(blank=True, null=True)
    martes_inicio_corte = models.IntegerField(blank=True, null=True)
    martes_final_corte = models.IntegerField(blank=True, null=True)
    miercoles_inicio = models.IntegerField(blank=True, null=True)
    miercoles_final = models.IntegerField(blank=True, null=True)
    miercoles_inicio_corte = models.IntegerField(blank=True, null=True)
    miercoles_final_corte = models.IntegerField(blank=True, null=True)
    jueves_inicio = models.IntegerField(blank=True, null=True)
    jueves_final = models.IntegerField(blank=True, null=True)
    jueves_inicio_corte = models.IntegerField(blank=True, null=True)
    jueves_final_corte = models.IntegerField(blank=True, null=True)
    viernes_inicio = models.IntegerField(blank=True, null=True)
    viernes_final = models.IntegerField(blank=True, null=True)
    viernes_inicio_corte = models.IntegerField(blank=True, null=True)
    viernes_final_corte = models.IntegerField(blank=True, null=True)
    sabado_inicio = models.IntegerField(blank=True, null=True)
    sabado_final = models.IntegerField(blank=True, null=True)
    sabado_inicio_corte = models.IntegerField(blank=True, null=True)
    sabado_final_corte = models.IntegerField(blank=True, null=True)
    domingo_inicio = models.IntegerField(blank=True, null=True)
    domingo_final = models.IntegerField(blank=True, null=True)
    domingo_inicio_corte = models.IntegerField(blank=True, null=True)
    domingo_final_corte = models.IntegerField(blank=True, null=True)
    filetype = models.CharField(max_length=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sucursales'


class TiposInscripciones(models.Model):
    nombre = models.CharField(max_length=250)
    descripcion = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tipos_inscripciones'

class Usuarios_portal_admin(models.Model):
    id_usuario = models.IntegerField()
    rol  = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'usuarios_portal_admin'