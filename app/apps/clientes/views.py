

# Create your views here.
import os, sys
from django.shortcuts import render
from django.http import HttpResponse,JsonResponse,HttpResponseBadRequest
from administrationProfile.models import *
import json
# import sys
from django.views.decorators.csrf import csrf_protect
import json
from django.db.models import Q
from django.core.paginator import Paginator
from generic.views   import *
import datetime  
from datetime import timedelta 
from django.utils import timezone
from django.db import connection
import MySQLdb as mdb
from urllib.request import urlopen

from django.db.models import ForeignKey
from django.contrib.auth.decorators import login_required
from generic.views import *
from vendedores.models import * 

from generic.decorators import * 

@login_required(login_url='customLogin')
@permitir_secciones_usuarios(grupos_permitidos=['Administrador','Supervisor'])
def listado_clientes(request):
    estados = Estados.objects.all()
    clientes = Clientes.objects.all().order_by('nombre');
    paginator = Paginator(clientes, 51) # Show 25 contacts per page.
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    
    datos = {"clientes":page_obj,"estados":estados}
    return render(request,"listado_clientes.html",datos)


@login_required(login_url='customLogin')
@permitir_secciones_usuarios(grupos_permitidos=['Administrador','Supervisor'])
def cliente(request , id_cliente):
    cliente = ""
    categoria = ""
    try:
        cliente =  Clientes.objects.get(id=id_cliente)
        categoria  =  ClientsCategories.objects.get(id=cliente.category_id)
        municipio  =  Municipios.objects.get(id = cliente.ciudad)
        estado  =  Estados.objects.get(clave= municipio.estado_id)
        total_publicaciones =  Publicaciones.objects.filter(id_cliente = cliente.id).count()
        tipo_inscripcion  =  TiposInscripciones.objects.get(id=cliente.tipo_inscripcion)

        datos = {"cliente":cliente,
        "categoria":categoria,
        "id_cliente":id_cliente,
        "existe":True,
        "municipio":municipio,
        "estado":estado,
        "total":total_publicaciones,
        "tipo_inscripcion":tipo_inscripcion}
        

    except Clientes.DoesNotExist:
        datos = {"existe": False}
    
    # query  =  cliente.query
    
    return render(request,"cliente.html",datos)


@login_required(login_url='customLogin')
@permitir_secciones_usuarios(grupos_permitidos=['Administrador','Supervisor'])
def publicaciones_por_cliente(request,id_cliente):
    publicaciones =  Publicaciones.objects.filter(id_cliente = id_cliente)


    for publicacion in publicaciones:
        # publicaciones.cancelada = "hola"

        if  Publicaciones_canceladas.objects.filter(publicacion_id = publicacion.id).exists():
            publicacion.revisada = "Cancelada"

        if  Publicaciones_revision.objects.filter(publicacion_id = publicacion.id).exists():
            publicacion.revisada = "Revision"

        if publicacion.revisada  ==  1:
            publicacion.revisada = "Aceptada"
 
        # else:
        #     publicacion.aceptada = False


    datos =  {"publicaciones":publicaciones}
    return render(request, "publicaciones_clientes.html", datos)
        

def buscador_clientes(request):
    
    if not request.is_ajax():
        datos={"status":"error","message":"Este metodo debe ser llamado por ajax"}
        return HttpResponseBadRequest(json.dumps(datos))
    
    if request.method  == "POST":
        palabra =  request.POST['palabra']
        estado = request.POST['estado']
        
        cantidad = 0
        clientes = ""
        if estado  == "SINESTADO":
            if palabra.isdigit():
                clientes =  Clientes.objects.filter(id__icontains=palabra)[:30]
                cantidad =  Clientes.objects.filter(id__icontains=palabra).count()
            else:
                clientes =  Clientes.objects.filter(nombre__icontains=palabra)[:30]
                cantidad =  Clientes.objects.filter(nombre__icontains=palabra).count()

        else:
            ciudad  = request.POST['municipio']
            if ciudad ==  "SINCIUDAD":
                if palabra.isdigit():
                    clientes =  Clientes.objects.filter(id__icontains=palabra, estado=estado)[:30]
                    cantidad =  Clientes.objects.filter(id__icontains=palabra, estado=estado).count()
                else:
                    clientes =  Clientes.objects.filter(nombre__icontains=palabra, estado=estado)[:30]
                    cantidad =  Clientes.objects.filter(nombre__icontains=palabra, estado=estado).count()
            else:
                if palabra.isdigit():
                    clientes =  Clientes.objects.filter(id__icontains=palabra, estado=estado,ciudad=ciudad)[:30]
                    cantidad =  Clientes.objects.filter(id__icontains=palabra, estado=estado,ciudad=ciudad).count()
                else:
                    clientes =  Clientes.objects.filter(nombre__icontains=palabra, estado=estado,ciudad=ciudad)[:30]
                    cantidad =  Clientes.objects.filter(nombre__icontains=palabra, estado=estado,ciudad=ciudad).count()

        respuesta = []
        if cantidad  == 0:
            datos  = {"status":"error","message":"No se encontraron clientes  :("}
            return HttpResponse(json.dumps(datos))
        for cliente in clientes:
            cli = {}
            vendedores_asignados = validar_vendedor_asignado(cliente.id)
            cli = { "id":cliente.id,
                "nombre":reparar_utf8(cliente.nombre),
                "filetype":cliente.filetype,
                "vendedor_asignado":vendedores_asignados["asignado"],
                "vendedor":vendedores_asignados["vendedor"]}
            respuesta.append(cli)
        datos = {"status":"success","clientes":respuesta}
        return HttpResponse(json.dumps(datos))
    else:
        datos= {"status":"error", "message":"Bad request"}
        return HttpResponseBadRequest(json.dumps(datos))


def validar_vendedor_asignado(id_cliente):
    asignado =False
    vendedor = "No asignado"
    try:
        resultado  =  ClientesPorVendedor.objects.get(cliente_id=id_cliente)
        asignado = True
        id_vendedor =  resultado.vendedor_id
        try:
            datos_vendedor  =  Vendedores.objects.select_related('usuario').get(id=id_vendedor)
            nombre  =  datos_vendedor.usuario.first_name+" "+datos_vendedor.usuario.last_name
            vendedor = nombre
        except Vendedores.DoesNotExist:
            vendedor = "No asignado"

        

    except ClientesPorVendedor.DoesNotExist:
        asignado = False
        vendedor = "No asignado"

    datos = {"asignado":asignado,"vendedor":vendedor}
    return datos

def get_municipios(request):
    if not request.is_ajax():
        datos = {"estatus":"error", "message":"debe ser una peticion ajax"}
        return JsonResponse(datos)
    
    if request.method == "POST":
        estado  =  request.POST["estado"]
        municipios  =  Municipios.objects.filter(estado_id=estado)
        datos_municipios = []
        for municipio in municipios:
            muni =  {}
            muni = {"id_municipio":municipio.id,
            "nombre":reparar_utf8(municipio.name)}
            datos_municipios.append(muni)
        
        return HttpResponse(json.dumps(datos_municipios))
            
    else:
        datos= {"status":"error", "message":"Bad request"}
        return HttpResponseBadRequest(json.dumps(datos))
